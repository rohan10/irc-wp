<?php /* Template Name: Home */
get_header(); 	
$options = get_option(SH_NAME.'_theme_options');
$home_sections = sh_set(sh_set($options , 'one_page_section') , 'one_page_section') ;
$count = 1;
foreach((array)$home_sections as $home_section):
	if(sh_set($home_section , 'tocopy')) break;
?>

	<?php
		$section_templates = sh_set($home_section , 'section_page');
		
		$total_tem = count($section_templates);
		for($i = 0 ; $i <= $total_tem ; $i++){
	
			get_template_part( 'templates/'.sh_set($section_templates, $i) );
		}
	?>
<?php
		$count++;
	endforeach;
get_footer(); 
?>