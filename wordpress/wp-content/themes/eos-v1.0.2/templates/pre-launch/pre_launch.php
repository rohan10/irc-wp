<!DOCTYPE html>
<html lang="en">
<?php
	$options = get_option(SH_NAME.'_theme_options') ;
	$social_icons = sh_set(sh_set($options , 'add_social_media_prelaunch') , 'add_social_media_prelaunch');
?>
	<head>
		<meta charset="UTF-8" />

		<!-- METADATA -->
		<meta name="description" content="A Responsive Bootstrap App Landing Page Template" />
		<meta name="keywords" content="EOS, Bootstrap, Landing page, Template, App, Mobile, Android, iOS" />
		<meta name="author" content="David Rozando" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

		<!-- PAGE TITLE -->
		<title><?php wp_title('-', true , 'right'); ?></title>

		<!-- FAVICON -->
		<link rel="icon" href="<?php echo get_template_directory_uri('template_url');?>/templates/pre-launch/images/favicons/favicon.ico" />
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri('template_url');?>/templates/pre-launch/images/favicons/apple-touch-icon.png" />
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri('template_url');?>/templates/pre-launch/images/favicons/apple-touch-icon-72x72.png" />
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri('template_url');?>/templates/pre-launch/images/favicons/apple-touch-icon-114x114.png" />

		<!--
		=================================
		STYLESHEETS
		=================================
		-->

		<!-- BOOTSTRAP -->
		<link rel="stylesheet" href="<?php echo get_template_directory_uri('template_url');?>/templates/pre-launch/css/bootstrap.min.css" />

		<!-- WEB FONTS -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic" />

		<!-- ICON FONTS -->
		<link rel="stylesheet" href="<?php echo get_template_directory_uri('template_url');?>/templates/pre-launch/css/font-awesome.min.css" />
		<link rel="stylesheet" href="<?php echo get_template_directory_uri('template_url');?>/templates/pre-launch/css/simple-line-icons.min.css" />

		<!-- OTHER STYLES -->
		<link rel="stylesheet" href="<?php echo get_template_directory_uri('template_url');?>/templates/pre-launch/css/animate.min.css" />

		<!-- MAIN STYLES -->
		<link rel="stylesheet" href="<?php echo get_template_directory_uri('template_url');?>/templates/pre-launch/css/style.css" />

		<!-- COLORS -->
		<link id="color-css" rel="stylesheet" href="<?php echo get_template_directory_uri('template_url');?>/templates/pre-launch/css/colors/green.css" />
		<!-- <link id="color-css" rel="stylesheet" href="css/colors/turquoise.css" /> -->
		<!-- <link id="color-css" rel="stylesheet" href="css/colors/blue.css" /> -->
		<!-- <link id="color-css" rel="stylesheet" href="css/colors/purple.css" /> -->
		<!-- <link id="color-css" rel="stylesheet" href="css/colors/pink.css" /> -->
		<!-- <link id="color-css" rel="stylesheet" href="css/colors/red.css" /> -->
		<!-- <link id="color-css" rel="stylesheet" href="css/colors/orange.css" /> -->
		<!-- <link id="color-css" rel="stylesheet" href="css/colors/yellow.css" /> -->

		<!-- JQUERY -->
		<script src="<?php echo get_template_directory_uri('template_url');?>/templates/pre-launch/js/jquery-1.11.1.min.js"></script>

	</head>

	<body>

		<div id="document" class="document">

			<!--
			=================================
			HERO SECTION
			=================================
			-->
			<section id="home" style="background-image: url(<?php echo sh_set($options , 'pre_launch_bg_image'); ?>);" class="hero-section hero-layout-1 section-inverse-color section">

				<!-- BACKGROUND OVERLAY -->
				<div class="black-background-overlay"></div>

				<div class="container">

					<div class="hero-content">

						<!-- HERO IMAGES -->
						<div class="hero-images">
						<?php
							$img_id1 = sh_get_attachment_id_by_url(sh_set($options , 'pre_launch_back_image'));
							$img_src1 = sh_set(wp_get_attachment_image_src( $img_id1 , '250x485') , 0);
							
							$img_id2 = sh_get_attachment_id_by_url(sh_set($options , 'pre_launch_front_image'));
							$img_src2 = sh_set(wp_get_attachment_image_src( $img_id2 , '260x540') , 0); 
						?>
							<img src="<?php echo esc_url($img_src1); ?>" class="hero-image phone-image-double phone-image-left wow fadeInLeft" alt="" data-wow-duration="1s" data-wow-delay="0.5s" />
							
							<img src="<?php echo esc_url($img_src2); ?>" class="hero-image phone-image-double phone-image-right phone-image-front wow fadeInLeft" alt="" data-wow-duration="1s" data-wow-delay="0s" />

						</div>

						<!-- HERO TEXT -->
						<div class="hero-text">
							
							<!-- LOGO -->
							<img src="<?php echo esc_url(sh_set($options , 'pre_launch_logo_image')); ?>" alt="EOS - App Landing Page Template" class="hero-logo" />

							<!-- TAGLINE -->
							<h1 class="hero-title"><?php echo sh_set($options , 'pre_launch_header_title'); ?></h1>

							<!-- HERO DESCRIPTION -->
							<p class="hero-description"><?php echo sh_set($options , 'pre_launch_text'); ?></p>

							<!-- SUBSCRIBE FORM -->
							<?php if(sh_set($options , 'news_letter_selection') == 'mailchimp'){ ?>
							<?php echo do_shortcode('[mc4wp_form]'); ?>
							<?php }
                                else{
                            ?>	
                         <form action="http://feedburner.google.com/fb/a/mailverify" method="post" class="subscribe-form wow fadeIn" data-wow-duration="1s" role="form">
                            <div class="form-validation alert"><!-- Validation Message here --></div>
                            <div class="form-group subscribe-form-input">
                                <input type="email" name="email" class="subscribe-form-email form-control form-control-lg" placeholder="<?php _e("Enter your email address" , SH_NAME) ; ?>" autocomplete="off" />
                                <input type="hidden" id="uri" name="uri" value="<?php echo sh_set($options , 'newsletter_signup_link'); ?>">
                                <input type="hidden" value="en_US" name="loc">
                                <button type="submit" class="subscribe-form-submit btn btn-black btn-lg" data-loading-text="<?php _e("Loading..." , SH_NAME) ; ?>"><?php echo (sh_set($options , 'newsletter_signup_button_text')) ? sh_set($options , 'newsletter_signup_button_text') : __("Subscribe" , SH_NAME) ; ?></button>
                            </div>
                        </form>
                        <?php }?>						
							<!-- HERO DESCRIPTION -->
							<p class="hero-release">
							<?php
								foreach($social_icons as $social_icon):
									if(sh_set($social_icon , 'tocopy')) break;
									$icon = str_replace( 'icon-', 'fa-', sh_set($social_icon , 'pre_launch_font_awesome') );
							?>
								<a href="<?php echo sh_set($social_icon , 'pre_launch_font_awesome_link'); ?>" style="color:#CCC">
									<i class="fa <?php echo esc_html($icon); ?>"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								</a>
							<?php endforeach; ?>
							</p>

						</div>
					</div>

					<!--
					=================================
					FOOTER SECTION
					=================================
					-->
					<footer class="footer-section section">
						<!-- COPYRIGHT -->
						<div class="copyright"><?php echo sh_set($options , 'copyright_text'); ?></div>
					</footer>

				</div>
			</section>
			

		</div>

		<!--
		=================================
		JAVASCRIPTS
		=================================
		-->
		<script src="<?php echo get_template_directory_uri('template_url');?>/templates/pre-launch/js/bootstrap.min.js"></script>
		<script src="<?php echo get_template_directory_uri('template_url');?>/templates/pre-launch/js/retina.min.js"></script>
		<script src="<?php echo get_template_directory_uri('template_url');?>/templates/pre-launch/js/smoothscroll.min.js"></script>
		<script src="<?php echo get_template_directory_uri('template_url');?>/templates/pre-launch/js/wow.min.js"></script>
		<script src="<?php echo get_template_directory_uri('template_url');?>/templates/pre-launch/js/script.js"></script>

	</body>

</html>