<?php 
	$options = get_option(SH_NAME.'_theme_options') ; 
	//printr($options);
	$invers_class = (sh_set($options , 'default_color_scheme') == 'light') ? 'section-inverse-color' : ' ' ;
?>
<section id="subscribe" class="subscribe-section section parallax-background <?php echo esc_html($invers_class) ; ?>" data-stellar-background-ratio="0.4" style="background-image: url(<?php echo esc_url(sh_set($options , 'newsletter_signup_section_bg'));?>);">

	<!-- BACKGROUND OVERLAY -->
	<div class="black-background-overlay"></div>

	<div class="container">

		<!-- SECTION HEADING -->
		<h2 class="section-heading text-center wow fadeIn" data-wow-duration="1s"><?php echo sh_set($options , 'newsletter_signup_heading'); ?></h2>
		<div class="row">
			<div class="col-md-6 col-md-offset-3 text-center">
				<p class="wow fadeIn" data-wow-duration="1s"><?php echo sh_set($options , 'newsletter_signup_tagline'); ?></p>
			</div>
		</div>

		<!-- SUBSCRIBE FORM -->
		<div class="row">
			<?php if(sh_set($options , 'news_letter_selection') == 'mailchimp'){ ?>
				<div class="col-md-6 col-md-offset-3 subscribe-form wow fadeIn">
					<?php echo do_shortcode('[mc4wp_form]'); ?>
				</div>
			<?php }
				else{
			?>
			<div class="col-md-6 col-md-offset-3">
				<form action="http://feedburner.google.com/fb/a/mailverify" method="post" class="subscribe-form wow fadeIn" data-wow-duration="1s" role="form">
					<div class="form-validation alert"><!-- Validation Message here --></div>
					<div class="form-group subscribe-form-input">
						<input type="email" name="email" id="mailchimp_email" class="subscribe-form-email form-control form-control-lg" placeholder="<?php _e("Enter your email address" , SH_NAME) ; ?>" autocomplete="off" />
						<input type="hidden" id="uri" name="uri" value="<?php echo sh_set($options , 'newsletter_signup_link'); ?>">
						<input type="hidden" value="en_US" name="loc">
						<button type="submit" class="subscribe-form-submit btn btn-black btn-lg" data-loading-text="<?php _e("Loading..." , SH_NAME) ; ?>"><?php _e("Subscribe" , SH_NAME) ; ?></button>
					</div>
				</form>
			</div>
			<?php }?>
		</div>
		
	</div>
</section>