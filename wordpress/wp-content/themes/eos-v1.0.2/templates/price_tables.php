<?php 
	$options = get_option(SH_NAME.'_theme_options') ;
	$ffeatures = sh_set(sh_set($options , 'pricing_table_features') , 'pricing_table_features') ;
	$add_pricing_tables = sh_set(sh_set($options , 'add_pricing_table'), 'add_pricing_table') ;
	
?>
<section id="pricing" class="pricing-section section">
	<div class="container">

		<!-- SECTION HEADING -->
		<h2 class="section-heading text-center wow fadeIn" data-wow-duration="1s">
			<?php echo sh_set($options , 'pricing_table_heading'); ?>
		</h2>
		<div class="row">
			<div class="col-md-6 col-md-offset-3 text-center">
				<p class="wow fadeIn" data-wow-duration="1s">
					<?php echo sh_set($options , 'pricing_table_tagline'); ?>
				</p>
			</div>
		</div>

		<!-- PRICING TABLE -->
		<ul class="pricing-table row wow bounceIn" data-wow-duration="1s">
			<!-- PRICING PACKAGE -->
			<?php 
				foreach((array)$add_pricing_tables as $add_pricing_table): 
				$features = sh_set($add_pricing_table , 'pt_features_on' );
				if(sh_set($add_pricing_table , 'tocopy')) break;
				$popular = (sh_set($add_pricing_table , 'pt_popular'))?'pricing-package-featured':'';
			?>
			<li class="col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-2">
				<div class="pricing-package <?php echo esc_html($popular) ; ?>">
					<div class="pricing-package-strip"></div>
					<div class="pricing-package-header">
						<h4>
							<?php echo sh_set($add_pricing_table  , 'pt_title'); ?>
							<?php if(sh_set($add_pricing_table , 'pt_popular')): ?> 
							<span class="label label-warning"><?php _e("Popular" , SH_NAME) ; ?></span>
							<?php endif; ?>
						</h4>
						<p><?php echo sh_set($add_pricing_table , 'pt_tagline'); ?></p>
						<div class="price">
						 <span class="price-currency">$</span>
						 <span class="price-number"><?php echo sh_set($add_pricing_table , 'pt_price'); ?></span>
						 <span class="price-period"><?php echo sh_set($add_pricing_table , 'pt_duration'); ?></span>
						</div>
					</div>
					<ul class="pricing-package-items">
					<?php 
					$count = 1 ; 
					foreach((array)$ffeatures as $feature): 
					if(sh_set($feature , 'tocopy')) break ;
					?>
						<?php $class = (in_array('feature'.$count , $features)) ? 'fa-check': 'fa-times' ;?>
						<li><i class="fa <?php echo esc_html($class); ?>"></i>
						<?php echo sh_set($feature , 'pt_feature_title');  ?></li>
					<?php 
					$count++ ; 
					endforeach; ?>									
					</ul>
				</div>
			</li>
			<?php endforeach; ?>
		</ul>

	</div>
</section>    