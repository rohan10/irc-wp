<?php 
	$options = get_option(SH_NAME.'_theme_options') ;
	
	$list_items = sh_set(sh_set($options , 'list_items'), 'list_items') ;
	
	$img_id1 = sh_get_attachment_id_by_url(sh_set($options , 'long_desc_image1'));
	
	$img_src1 = sh_set(wp_get_attachment_image_src( $img_id1 , '250x485') , 0);
	
	$img_id2 = sh_get_attachment_id_by_url(sh_set($options , 'long_desc_image2'));
	
	$img_src2 = sh_set(wp_get_attachment_image_src( $img_id2 , '260x540') , 0);
	
?>

<!--
=================================
DESCRIPTION SECTION
=================================
-->
<section id="description" class="description-section section">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <!-- SECTION HEADING -->
                <h2 class="section-heading wow fadeIn" data-wow-duration="1s">
                    <?php echo sh_set($options , 'long_desc_heading'); ?>
                </h2>
                <p class="wow fadeIn" data-wow-duration="1s">
                    <?php echo sh_set($options , 'long_desc'); ?> 
                </p>

                <!-- ICON LIST -->
                <ul class="list-with-icons wow fadeIn" data-wow-duration="1s">
                	<?php foreach($list_items as $item): ?>
                    
                    <li>
                        <i class="<?php echo sh_set($item , 'list_item_icon'); ?>"></i>
                        <?php echo sh_set($item , 'list_item_title'); ?>
                    </li>
                    
                    <?php endforeach ; ?>
                    
                </ul>

            </div>

            <!-- PHONE IMAGE -->
            <div class="col-md-5 text-center">

                <img src="<?php echo esc_url($img_src1); ?>" class="description-image phone-image-double phone-image-left wow fadeInRight" alt="" data-wow-duration="1s" data-wow-delay="0.5s" />
                <img src="<?php echo esc_url($img_src2); ?>" class="description-image phone-image-double phone-image-right phone-image-front wow fadeInRight" alt="" data-wow-duration="1s" data-wow-delay="1.5s" />

            </div>
        </div>

    </div>
</section>