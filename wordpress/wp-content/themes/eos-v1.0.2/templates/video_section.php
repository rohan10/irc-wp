<?php 
	$options = get_option(SH_NAME.'_theme_options') ;
	
	if(strstr(sh_set($options , 'video_link'),"vimeo.com")){
		
		$video_src = str_replace( "vimeo.com" , "player.vimeo.com/video" , sh_set($options , 'video_link') );
	}
	
	if(strstr(sh_set($options , 'video_link'),"youtube.com")){
	
		$video_src = str_replace( "watch?v=" , "embed/" , sh_set($options , 'video_link') );
	}
	if(sh_set($options , 'video_link')):
?>

<!--
=================================
VIDEO SECTION
=================================
-->
<section id="video" class="video-section section parallax-background" data-stellar-background-ratio="0.4" style="background-image: url(<?php echo sh_set($options , 'video_section_bg');?>);">
    <!-- BACKGROUND OVERLAY -->
    <div class="black-background-overlay"></div>

    <div class="container">

        <div class="row">
            <div class="col-md-10 col-md-offset-1" style="border:0px !important;">

                <div class="video-embed wow fadeIn" data-wow-duration="1s">
				
				<?php if(strstr(sh_set($options , 'video_link'),"vimeo.com")){ ?>

                    <!-- VIDEO EMBED FROM VIMEO: please change the URL -->
                    <iframe src="<?php echo esc_url($video_src); ?>?title=0&amp;byline=0&amp;portrait=0&amp;color=8aba56" width="500" height="281" allowfullscreen></iframe>
					
				<?php	
					}
					if(strstr(sh_set($options , 'video_link'),"youtube.com")){ ?>
                    
                    <!-- VIDEO EMBED FROM YOUTUBE: please change the URL -->
                   <iframe width="560" height="315" src="<?php echo esc_url($video_src); ?>?rel=0" allowfullscreen></iframe>
				<?php	}?>

                </div>
            </div>
        </div>

    </div>
</section>
<?php endif; ?>