<?php 
	$options = get_option(SH_NAME.'_theme_options') ;
			
	$features_with_icons = sh_set(sh_set($options , 'features_with_icons') , 'features_with_icons'); 
	
	$img_id = sh_get_attachment_id_by_url(sh_set($options , 'features_with_icons_image'));
	
	$img_src = sh_set(wp_get_attachment_image_src( $img_id , '265x550') , 0);
					

?>

<!--
=================================
FEATURES WITH ICONS SECTION
=================================
-->
<section id="features-icons" class="features-icons-section section">
	<div class="container">

		<div class="row">
			<div class="col-md-8 col-md-push-4">

				<!-- SECTION HEADING -->
				<h2 class="section-heading wow fadeIn" data-wow-duration="1s">
				<?php echo sh_set($options , 'features_with_icons_heading'); ?>
				</h2>
				<p class="wow fadeIn" data-wow-duration="1s">
				<?php echo sh_set($options , 'features_with_ions_tagline'); ?>
				</p>

				<!-- FEATURES LIST WITH ICONS -->
				<div class="features-icon-list row wow fadeIn" data-wow-duration="1s">
					
					<?php $count = 1 ; ?>
					<?php foreach($features_with_icons as $feature): ?>
					<?php if(sh_set($feature , 'tocopy')) break ; ?>
					
					<div class="features-icon-list-item col-sm-6">
						<i class="<?php echo sh_set($feature , 'feature_with_icon_icon'); ?>"></i>
						<h5><?php echo sh_set($feature , 'feature_with_icon_title'); ?></h5>
						<p><?php echo sh_set($feature , 'feature_with_icon_desc'); ?></p>
					</div>                               
					
					<?php if($count == 2 ) : ?><div class="clear"></div> <?php endif; ?>
					
					 <?php $count = ($count < 2 )? $count + 1 : 1 ; ?>
					<?php endforeach ; ?>
					
				</div>
			</div>

			<!-- PHONE IMAGE -->
			<div class="col-md-4 col-md-pull-8 text-center">
				<img src="<?php echo esc_url($img_src); ?>" class="features-icons-image wow fadeInLeft" alt="" data-wow-duration="1s" />
			</div>
		</div>

	</div>
</section>