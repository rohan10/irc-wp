<?php 
	$options = get_option(SH_NAME.'_theme_options') ;
	
	$layout = sh_set($options , 'theme_layouts'); 
	
	$close_up_features = sh_set(sh_set($options , 'close_up_features') , 'close_up_features'); 
	
	$last = count($close_up_features) - 1; 
	
	unset($close_up_features[$last]) ;
	
	$chunks = array_chunk($close_up_features , (int)ceil($last/2) );
	
	$img_id = sh_get_attachment_id_by_url(sh_set($options , 'close_up_features_image'));
	
	$img_src = sh_set(wp_get_attachment_image_src( $img_id , '310x530') , 0);
?>

<section id="features" class="features-section section">
	<div class="container">

		<!-- SECTION HEADING -->
		<h2 class="section-heading text-center wow fadeIn" data-wow-duration="1s">
		<?php echo sh_set($options , 'close_up_features_heading'); ?> </h2>
		<div class="row">
			<div class="col-md-6 col-md-offset-3 text-center">
				<p class="wow fadeIn" data-wow-duration="1s">
				<?php echo sh_set($options , 'close_up_features_tagline'); ?></p>
			</div>
		</div>

		<div class="featuers-list-wrapper row">

			<!-- FEATURES LEFT -->
			<?php $count = 1 ; ?>
			<?php foreach($chunks as $chunk): ?>
			<?php $div_class = ($count == 1) ? 'col-md-4 col-sm-6' : 'col-md-4 col-md-push-4 col-sm-6' ; ?>
			<?php $ul_class = ($count == 1)?'features-list-left fadeInLeft':'features-list-right fadeInRight'; ?>
			<div class="<?php echo esc_html($div_class); ?>">
				<ul class="features-list wow <?php echo esc_html($ul_class); ?>" data-wow-duration="1s">
					<?php foreach($chunk as $c): ?>
					<li class="features-list-item">
						<h5><?php echo sh_set($c , 'closeup_feature_title'); ?></h5>
						<p><?php echo sh_set($c , 'closeup_feature_desc'); ?></p>
					</li>
					<?php endforeach; ?>
				</ul>
			</div>
			<?php $count++ ; ?>
			<?php endforeach; ?>
			
			<?php if($img_src): ?>
			<!-- CLOSE UP PHONE IMAGE -->
			<div class="col-md-4 col-md-pull-4 text-center">
				<img src="<?php echo esc_url($img_src); ?>" class="features-image wow fadeInUp" alt="" data-wow-duration="1s" />
			</div>
			<?php endif; ?>

		</div>

	</div>
</section>