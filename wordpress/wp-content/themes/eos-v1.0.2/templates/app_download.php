<?php 
	$options = get_option(SH_NAME.'_theme_options') ;
	$link_images = sh_set( sh_set($options, 'link_images') , 'link_images' );
	$layout = sh_set($options , 'theme_layouts'); 
	$invers_class = (sh_set($options , 'default_color_scheme') == 'light') ? 'section-inverse-color' : ' ' ;
	$black_overlay = (sh_set($options , 'section_bg_option' ) == 'video' ) ? 'black-dot-background-overlay' : 'black-background-overlay' ;
	//printr($options);
?>
<section id="home" class="hero-section hero-layout-1 section parallax-background <?php echo esc_html($invers_class); ?>"   data-stellar-background-ratio="0.4" <?php if(sh_set($options , 'section_bg_option' )=='image'): ?> style="background-image: url(<?php echo sh_set($options , 'app_download_section_bg') ?>);"<?php endif; ?> >
	
	<?php if(sh_set($options , 'section_bg_option' )=='video'): ?>
	<!-- VIDEO BACKGROUND -->
	<div class="video-background-container parallax" data-stellar-ratio="0.4">
		<video preload="auto" autoplay loop muted class="video-background">
			<source type="video/mp4" src="<?php echo sh_set($options , 'app_download_section_video_bg');?>" />
		</video>
	</div>
	<?php endif; ?>
	
	<div class="<?php echo esc_html($black_overlay) ; ?>"></div>
    <div class="container">
    	<div class="hero-content" <?php if($layout == 'layout_1' || $layout == 'layout_4' ) : ?>data-stellar-offset-parent='true'<?php endif; ?>>
<?php 
switch ($layout) :

  case "layout_1": ?>
  
            <!-- HERO TEXT -->
            <div class="hero-text wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                <!-- LOGO -->
                <img src="<?php echo sh_set($options , 'app_logo_l1'); ?>" alt="EOS - App Landing Page Template" class="hero-logo" />

                <!-- TAGLINE -->
                <h1 class="hero-title"><?php echo sh_set($options , 'app_download_heading_l1'); ?></h1>

                <!-- HERO DESCRIPTION -->
                <p class="hero-description"><?php echo sh_set($options , 'app_download_description_l1'); ?></p>

                <!-- DOWNLOAD BUTTONS -->
                <p class="download-buttons">
                    <!-- APP STORE DOWNLOAD -->
                    <?php if(sh_set($options , 'app_store_links_l1')): ?>
                    <a href="<?php echo sh_set($options , 'app_store_link'); ?>" class="btn btn-app-download btn-ios">
                        <i class="fa fa-apple"></i>
                        <strong><?php _e("Download App" , SH_NAME); ?></strong> 
                        <span><?php _e("from App Store" , SH_NAME); ?></span>
                    </a>
                    <?php endif; ?>
                    <!-- PLAY STORE DOWNLOAD -->
                    <?php if(sh_set($options , 'play_store_links_l1')): ?>
                    <a href="<?php echo sh_set($options , 'play_store_link'); ?>" class="btn btn-app-download btn-primary">
                        <i class="fa fa-android"></i>
                        <strong><?php _e("Download App" , SH_NAME); ?></strong> 
                        <span><?php _e("from Play Store" , SH_NAME); ?></span>
                    </a>
                    <?php endif; ?>
					
					<?php	if(sh_set($options , 'win_store_links_l1')): ?>
					<!-- WINDOWS PHONE STORE DOWNLOAD -->
					<a href="<?php echo sh_set($options , 'win_store_link'); ?>" class="btn btn-app-download btn-windows-phone">
						<i class="fa fa-windows"></i>
						<strong><?php _e("Download App" , SH_NAME) ; ?></strong> 
						<span><?php _e("from Windows Store" , SH_NAME) ; ?></span>
					</a>
					<?php endif; ?>
					
					<?php
						if(sh_set($options , 'link_images_l1') > 0 ):
						$count = 1 ;
						foreach($link_images as $link_image) :
						if(sh_set($link_image , 'tocopy')) break;
						if(sh_set($link_image , 'link_uploaded_image')):
						$img_id = sh_get_attachment_id_by_url(sh_set($link_image , 'link_uploaded_image'));
						$img_src = sh_set(wp_get_attachment_image_src( $img_id , '216x62') , 0);
					?>
					<a href="<?php echo sh_set($link_image , 'uploaded_image_link'); ?>">
						<img src="<?php echo $img_src ; ?>" class="link_images" alt="" class="img-responsive" />
					</a>
					<?php
						if(sh_set($options , 'link_images_l1') == $count ) break;
						$count++;
						endif;
						endforeach;
						endif;
					?>
					
                </p>

            </div>

            <!-- HERO IMAGES -->
            <div class="hero-images wow layout3HeroPhone" data-wow-duration="1s" data-wow-delay="0.5s">
                <img src="<?php echo sh_set($options , 'app_left_image_l1'); ?>" class="hero-image parallax" alt="" data-stellar-ratio="0.7" />
            </div>
										
			
<?php
    break;
  case "layout_2": 
?>					

            <!-- HERO TEXT -->
            <div class="hero-text">
                
                <!-- LOGO -->
                <div class="hero-logo wow fadeIn" data-wow-duration="1s">
                    <img src="<?php echo sh_set($options , 'app_logo_l2'); ?>" alt="EOS - App Landing Page Template" />
                </div>

                <!-- TAGLINE -->
                <h1 class="hero-title wow fadeInUp" data-wow-duration="1s">
					<?php echo sh_set($options , 'app_download_heading_l2'); ?>
                </h1>

                <!-- DOWNLOAD BUTTONS -->
                <p class="download-buttons wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
                    <?php if(sh_set($options , 'app_store_links_l2')): ?>
                    <a href="<?php echo sh_set($options , 'app_store_link'); ?>" class="btn btn-app-download btn-ios">
                        <i class="fa fa-apple"></i>
                        <strong><?php _e("Download App" , SH_NAME); ?></strong> 
                        <span><?php _e("from App Store" , SH_NAME); ?></span>
                    </a>
                    <?php endif; ?>
                    <!-- PLAY STORE DOWNLOAD -->
                    <?php if(sh_set($options , 'play_store_links_l2')): ?>
                    <a href="<?php echo sh_set($options , 'play_store_link'); ?>" class="btn btn-app-download btn-primary">
                        <i class="fa fa-android"></i>
                        <strong><?php _e("Download App" , SH_NAME); ?></strong> 
                        <span><?php _e("from Play Store" , SH_NAME); ?></span>
                    </a>
                    <?php endif; ?>
					<?php	if(sh_set($options , 'win_store_links_l2')): ?>
					<!-- WINDOWS PHONE STORE DOWNLOAD -->
					<a href="<?php echo sh_set($options , 'win_store_link'); ?>" class="btn btn-app-download btn-windows-phone">
						<i class="fa fa-windows"></i>
						<strong><?php _e("Download App" , SH_NAME) ; ?></strong> 
						<span><?php _e("from Windows Store" , SH_NAME) ; ?></span>
					</a>
					<?php endif; ?>
					
					<?php
						if(sh_set($options , 'link_images_l2') > 0) :
						$count = 1 ;
						foreach($link_images as $link_image) :
						if(sh_set($link_image , 'tocopy')) break;
						if(sh_set($link_image , 'link_uploaded_image')):
						$img_id = sh_get_attachment_id_by_url(sh_set($link_image , 'link_uploaded_image'));
						$img_src = sh_set(wp_get_attachment_image_src( $img_id , '216x62') , 0);
					?>
					<a href="<?php echo sh_set($link_image , 'uploaded_image_link'); ?>">
						<img src="<?php echo $img_src ; ?>" class="link_images" alt="" class="img-responsive" />
					</a>
					<?php
						if(sh_set($options , 'link_images_l2') == $count ) break;
						$count++;
						endif;
						endforeach;
						endif;
					?>
					
                </p>

				<?php if(sh_set($options , 'video_link')): ?>
                <!-- WATCH THE VIDEO -->
                <a href="#video" class="hero-watch-video-link anchor-link wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
                <i class="fa fa-youtube-play"></i><?php _e("Watch The Video" , SH_NAME); ?></a>
                
                <?php endif; ?>

            </div>

<?php 
    break;
  case "layout_3": ?>
  
            <!-- HERO TEXT -->
            <div class="hero-text wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                
                <!-- LOGO -->
                <img src="<?php echo sh_set($options , 'app_logo_l3'); ?>" alt="EOS - App Landing Page Template" class="hero-logo" />

                <!-- TAGLINE -->
                <h1 class="hero-title">
                <?php echo sh_set($options , 'app_download_heading_l3'); ?>
                </h1>

                <!-- HERO DESCRIPTION -->
                <p class="hero-description"><?php echo sh_set($options , 'app_download_description_l3'); ?></p>

                <!-- DOWNLOAD BUTTONS -->
                <p class="download-buttons">
                    <!-- APP STORE DOWNLOAD -->
                    <?php if(sh_set($options , 'app_store_links_l3')): ?>
                    <a href="<?php echo sh_set($options , 'app_store_link'); ?>" class="btn btn-app-download btn-ios">
                        <i class="fa fa-apple"></i>
                        <strong><?php _e("Download App" , SH_NAME); ?></strong> 
                        <span><?php _e("from App Store" , SH_NAME); ?></span>
                    </a>
                    <?php endif; ?>
                    <!-- PLAY STORE DOWNLOAD -->
                    <?php if(sh_set($options , 'play_store_links_l3')): ?>
                    <a href="<?php echo sh_set($options , 'play_store_link'); ?>" class="btn btn-app-download btn-primary">
                        <i class="fa fa-android"></i>
                        <strong><?php _e("Download App" , SH_NAME); ?></strong> 
                        <span><?php _e("from Play Store" , SH_NAME); ?></span>
                    </a>
                    <?php endif; ?>
					<?php	if(sh_set($options , 'win_store_links_l3')): ?>
					<!-- WINDOWS PHONE STORE DOWNLOAD -->
					<a href="<?php echo sh_set($options , 'win_store_link'); ?>" class="btn btn-app-download btn-windows-phone">
						<i class="fa fa-windows"></i>
						<strong><?php _e("Download App" , SH_NAME) ; ?></strong> 
						<span><?php _e("from Windows Store" , SH_NAME) ; ?></span>
					</a>
					<?php endif; ?>
					
					<?php
						if(sh_set($options , 'link_images_l3') > 0) :
						$count = 1 ;
						foreach($link_images as $link_image) :
						if(sh_set($link_image , 'tocopy')) break;
						if(sh_set($link_image , 'link_uploaded_image')):
						$img_id = sh_get_attachment_id_by_url(sh_set($link_image , 'link_uploaded_image'));
						$img_src = sh_set(wp_get_attachment_image_src( $img_id , '216x62') , 0);
					?>
					<a href="<?php echo sh_set($link_image , 'uploaded_image_link'); ?>">
						<img src="<?php echo $img_src ; ?>" class="link_images" alt="" class="img-responsive" />
					</a>
					<?php
						if(sh_set($options , 'link_images_l3') == $count ) break;
						$count++;
						endif;
						endforeach;
						endif;
					?>
                </p>

            </div>
            <!-- HERO IMAGES -->
            <div class="hero-images">

                <img src="<?php echo sh_set($options , 'app_left_image1_l3'); ?>" class="hero-image phone-image-double phone-image-left wow fadeInRight" alt="" data-wow-duration="1s" data-wow-delay="0.5s" />
				
                <img src="<?php echo sh_set($options , 'app_left_image2_l3'); ?>" class="hero-image phone-image-double phone-image-right phone-image-front wow fadeInRight" alt="" data-wow-duration="1s" data-wow-delay="1.5s" />

            </div>

  
<?php 
    break;
	  case "layout_4": ?>

			<!-- HERO TEXT -->
			<div class="hero-text wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
				
				<!-- LOGO -->
				<img src="<?php echo sh_set($options , 'app_logo_l4'); ?>" alt="" class="hero-logo" />

				<!-- TAGLINE -->
				<h1 class="hero-title"><?php echo sh_set($options , 'app_download_heading_l4'); ?></h1>

				<!-- HERO DESCRIPTION -->
				<p class="hero-description"><?php echo sh_set($options , 'app_download_description_l4'); ?></p>

				<!-- DOWNLOAD BUTTONS -->
				<p class="download-buttons">
				
					<?php if(sh_set($options , 'app_store_links_l4')): ?>
					<!-- APP STORE DOWNLOAD -->
					<a href="<?php echo sh_set($options , 'app_store_link'); ?>" class="btn btn-app-download btn-ios">
						<i class="fa fa-apple"></i>
						<strong><?php _e("Download App" , SH_NAME) ; ?></strong> 
						<span><?php _e("from App Store" , SH_NAME) ; ?></span>
					</a>
					<?php
						endif;
						if(sh_set($options , 'play_store_links_l4')):
					?>
					<!-- PLAY STORE DOWNLOAD -->
					<a href="<?php echo sh_set($options , 'play_store_link'); ?>" class="btn btn-app-download btn-primary">
						<i class="fa fa-android"></i>
						<strong><?php _e("Download App" , SH_NAME) ; ?></strong> 
						<span><?php _e("from Play Store" , SH_NAME) ; ?></span>
					</a>
					<?php
						endif;
						if(sh_set($options , 'win_store_links_l4')):
					?>
					<!-- WINDOWS PHONE STORE DOWNLOAD -->
					<a href="<?php echo sh_set($options , 'win_store_link'); ?>" class="btn btn-app-download btn-windows-phone">
						<i class="fa fa-windows"></i>
						<strong><?php _e("Download App" , SH_NAME) ; ?></strong> 
						<span><?php _e("from Windows Store" , SH_NAME) ; ?></span>
					</a>
					<?php endif; ?>
					
					<?php
						if(sh_set($options , 'link_images_l4') > 0) :
						$count = 1 ;
						foreach($link_images as $link_image) :
						if(sh_set($link_image , 'tocopy')) break;
						if(sh_set($link_image , 'link_uploaded_image')):
						$img_id = sh_get_attachment_id_by_url(sh_set($link_image , 'link_uploaded_image'));
						$img_src = sh_set(wp_get_attachment_image_src( $img_id , '216x62') , 0);
					?>
					<a href="<?php echo sh_set($link_image , 'uploaded_image_link'); ?>">
						<img src="<?php echo $img_src ; ?>" class="link_images" alt="" class="img-responsive" />
					</a>
					<?php
						if(sh_set($options , 'link_images_l4') == $count ) break;
						$count++;
						endif;
						endforeach;
						endif;
					?>
					
				</p>

			</div>
			
			<?php if(sh_set($options , 'app_left_image_l4')): ?>
			<!-- HERO IMAGES -->
			<div class="hero-images wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
				<img src="<?php echo sh_set($options , 'app_left_image_l4'); ?>" class="hero-image parallax img-responsive" alt="" data-stellar-ratio="0.7" />
			<?php if(sh_set($options , 'video_link')): ?>
				<!-- WATCH THE VIDEO -->
				<a href="#video" class="hero-watch-video-link anchor-link"><i class="fa fa-play"></i><span class="hidden"><?php _e("Watch The Video" , SH_NAME) ; ?></span></a>
			</div>
			<?php endif; ?>
			
			<?php endif; ?>
					  
<?php 
    break;
	
endswitch ; 
?>
		</div>
	</div>
</section>
  