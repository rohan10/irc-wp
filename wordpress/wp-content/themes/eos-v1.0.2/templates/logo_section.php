<?php 
	$options = get_option(SH_NAME.'_theme_options') ; 
	$logo_images = sh_set(sh_set($options , 'logo_images') , 'logo_images') ;
?>
<section id="press" class="press-section section">
	<div class="container">

		<!-- LOGOS -->
		<div class="press-logos wow fadeIn" data-wow-duration="1s">
			<?php 
				foreach((array)$logo_images as $logo_image): 
				if(sh_set($logo_image , 'tocopy')) break;			
			?>
			<span><img src="<?php echo sh_set($logo_image , 'logo_image_sec'); ?>" alt="Logo" /></span>
			<?php endforeach; ?>
		</div>

	</div>
</section>