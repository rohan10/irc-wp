<?php $options = get_option(SH_NAME.'_theme_options') ; ?>
<?php if(sh_set($options , 'cs_form_include') == 'contact_form'): ?>
<section id="contact" class="contact-section section collapse">
	<div class="container">

		<!-- SECTION HEADING -->
		<h2 class="section-heading text-center wow fadeIn" data-wow-duration="1s"><?php echo sh_set($options , 'cs_contant_title'); ?></h2>
		<div class="row">
			<div class="col-md-6 col-md-offset-3 text-center">
				<p class="animated wow fadeIn" data-wow-duration="1s">
					<?php echo sh_set($options , 'cs_contant_description'); ?>
				</p>

			</div>
		</div>

		<!-- CONTACT FORM -->
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<form action="<?php echo admin_url('admin-ajax.php?action=_sh_ajax_callback&amp;subaction=sh_contact_form_submit'); ?>" method="post" id="eos_contact_form" class="contact-form wow fadeInUp" data-wow-duration="1s" role="form">
					<div class="msgs"><!-- Validation Message here --></div>
					
					<div class="form-group">
						<input type="text" class="form-control" name="contact_name" id="name" placeholder="<?php _e("Your Name" , SH_NAME); ?>" autocomplete="off">
					</div>
					
					<div class="form-group">
						<input type="email" class="form-control" name="contact_email" id="email" placeholder="<?php _e("Your Email Address" , SH_NAME); ?>" autocomplete="off">
					</div>
					
					<div class="form-group">
						<input type="text" class="form-control" name="contact_subject" id="subject" placeholder="<?php _e("Subject" , SH_NAME); ?>" autocomplete="off">
					</div>
					
					<div class="form-group">
						<textarea rows="4" class="form-control" name="contact_message" id="message" placeholder="<?php _e("Message" , SH_NAME); ?>"></textarea>
					</div>
					
					<div class="form-group">
						<button type="submit" id="contact_form_submit" name="contact_submit" class="btn btn-primary btn-block" data-loading-text="<?php _e("Loading..." , SH_NAME); ?>"><?php _e("Send Messag" , SH_NAME); ?></button>
					</div>
					
				</form>
			</div>
		</div>

	</div>
</section>
<script>
jQuery(document).ready(function($) {
	$('#eos_contact_form').live('submit', function(e){
	
		e.preventDefault();
		var thisform = this;
		var fields = $(this).serialize();
		var url= $(this).attr('action');
		//alert(url);
		$.ajax({
			url: url,
			type: 'POST',
			data: fields,
			success:function(res){
				//salert(res);
				$('.msgs', thisform).html(res);
			}
		});
	});
	

});
</script>
<?php endif; ?>
