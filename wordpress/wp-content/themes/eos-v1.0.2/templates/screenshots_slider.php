<?php 
	$options = get_option(SH_NAME.'_theme_options') ; 
	$screenshots_images = sh_set(sh_set($options , 'screenshots_image') , 'screenshots_image') ;
?>
<section id="screenshots" class="screenshots-section section">
	<div class="container">
		<!-- SECTION HEADING -->
		<h2 class="section-heading text-center wow fadeIn" data-wow-duration="1s"><?php echo sh_set($options , 'screenshots_slider_heading');?></h2>
		<div class="row">
			<div class="col-md-6 col-md-offset-3 text-center">
				<p class="wow fadeIn" data-wow-duration="1s"><?php echo sh_set($options , 'screenshots_slider_tagline');?></p>
			</div>
		</div>
		<!-- SCREENSHOT IMAGES -->
		<div class="sreenshots-carousel">
			<ul class="screenshot-images">
			<?php 
				foreach((array)$screenshots_images as $screenshots_image): 
				if(sh_set($screenshots_image , 'tocopy')) break;
				$img_id = sh_get_attachment_id_by_url(sh_set($screenshots_image , 'screenshots_slider_image'));
				$img_src = sh_set(wp_get_attachment_image_src( $img_id , '245x435') , 0);
			?>
				<li>
					<a href="<?php echo sh_set($screenshots_image , 'screenshots_slider_image');?>" class="screenshot-image" data-lightbox-gallery="screenshots-gallery">
						<div class="screenshot-image-inner">
							<img src="<?php echo esc_url($img_src) ; ?>" alt="<?php _e("screenshot" , SH_NAME ) ; ?>" />
							<div class="hover"><i class="icon-magnifier-add"></i></div>
						</div>
					</a>
				</li>
			<?php endforeach; ?>
			</ul>
		</div>
	</div>
</section>