<?php 
	$options = get_option(SH_NAME.'_theme_options') ;
	$testimonials = sh_set(sh_set($options , 'testimonials'), 'testimonials') ;
?>
<section id="testimonials" class="testimonials-section section parallax-background" data-stellar-background-ratio="0.4" style="background-image: url(<?php echo sh_set( $options , 'testimonials_section_bg' ); ?>);">
	<div class="accent-background-overlay"></div>
	<div class="container">
		<div class="testimonials-carousel">
			<ul class="testimonial-items">
				<?php 
					foreach((array)$testimonials as $testimonial): 
					if(sh_set($testimonial , 'tocopy')) break;
				?>
					<li>
						<div class="testimonial-text"><?php echo sh_set($testimonial , 'testimonial_text'); ?></div>
						<?php if(sh_set($testimonial  , 'testimonial_author') || sh_set($testimonial  , 'author_designation')): ?>
						<div class="testimonial-name">
							<?php echo sh_set($testimonial  , 'testimonial_author'); ?><?php if(sh_set($testimonial  , 'testimonial_author') && sh_set($testimonial  , 'author_designation')): ?>, <?php endif; ?><?php echo sh_set($testimonial  , 'author_designation'); ?>
						</div>
						<?php endif; ?>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
</section>