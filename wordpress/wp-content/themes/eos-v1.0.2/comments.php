<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains comments and the comment form.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

/*
 * If the current post is protected by a password and the visitor has not yet
 * entered the password we will return early without loading the comments.
 */
if ( post_password_required() )
	return;
	
if ( ! comments_open() && !get_comments_number() ) return;
?>

<div class="comments">
	<h3 class="blog-section-heading"><?php echo get_comments_number(); ?> <?php _e("Comments" , SH_NAME); ?></h3>
<?php if ( have_comments() ) : ?>
	<ul class="comments-list">
        <?php
			wp_list_comments( array(
				'style'       => 'ul',
				'short_ping'  => true,
				'avatar_size' => 71,
				'callback'=>'sh_list_comments',
				
			) );
		?>
     </ul>
	

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :?>
        
        <nav class="comments-pagination clearfix">
            <span class="comments-pagination-link-left">
			<?php next_comments_link( __( 'Newer Comments &rarr;', SH_NAME ) ); ?>
            </span>
            <span class="comments-pagination-link-right">
			<?php previous_comments_link( __( 'Older Comments &raquo;', SH_NAME ) ); ?> 
            </span>
        </nav>
			<!-- .comment-navigation -->
		<?php endif; // Check for comment navigation ?>

		<?php if ( ! comments_open() && get_comments_number() ) : ?>
			<p class="no-comments"><?php _e( 'Comments are closed.' , SH_NAME ); ?></p>
		<?php endif; ?>

	<?php endif; // have_comments() ?>
</div>
	<?php sh_comment_form(); ?>