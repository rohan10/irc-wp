<?php 
	$settings = get_option(SH_NAME.'_theme_options');
	if(!is_user_logged_in() && sh_set($settings , 'show_pre_launch_page')){
		get_template_part( 'templates/pre-launch/pre_launch' );
		exit;
	}
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	
	<!-- FAVICON -->
	<?php 		
		$home_sections = sh_set(sh_set($settings , 'one_page_section') , 'one_page_section') ;
		$one_page_section_links = sh_set(sh_set($settings , 'one_page_section_links') , 'one_page_section_links') ;
		
		echo ( sh_set( $settings, 'site_favicon' ) ) ? 
		'<link rel="icon" type="image/png" href="'.sh_set( $settings, 'site_favicon' ).'">' : '' ;
	?>
	
	<!-- PAGE TITLE -->
	<title><?php wp_title('-', true , 'right'); ?></title>

	

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php if(sh_set($settings , 'cs_preloader')): ?>
	<div id="preloader" class="preloader">
		<div class="preloader-inner">
			<span class="preloader-logo">
				<img src="<?php echo sh_set($settings , 'cs_preloader_image'); ?>" alt="" />
				<strong><?php echo sh_set($settings , 'cs_preloader_text'); ?></strong>
			</span>
		</div>
	</div>
<?php endif;?>
	<div id="document" class="document">
	<!--
	=================================
	HEADER
	=================================
	-->
	<?php 
		global $wp_query;
		$header_class = ((is_home() || is_front_page()) && !($wp_query->is_posts_page)) ? '' : 'header-fixed'; 
		$headerfloating = (sh_set($settings , 'header_float')) ? 'header-floating' : '' ;
		$style = '';
		if(sh_set($settings , 'header_float') && is_user_logged_in()) $style = 'style="top:32px;"' ;
		if(sh_set($settings , 'header_float') && !is_user_logged_in()) $style = '' ;
		if(!sh_set($settings , 'header_float')) $style = 'style="position:absolute;"' ;
	?>
		<header <?php echo $style; ?> class="header-section navbar navbar-fixed-top navbar-default <?php echo esc_html($headerfloating); ?> <?php echo esc_html($header_class); ?>">
			<div class="container">

				<div class="navbar-header">

					<!-- RESPONSIVE MENU BUTTON -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

					<!-- NAVBAR LOGO -->
					<a class="navbar-brand navbar-logo" href="<?php echo home_url(); ?>">
						<img src="<?php echo sh_set($settings , 'logo_image'); ?>" alt="" height="<?php echo sh_set($settings , 'logo_height') ?>" width="<?php echo sh_set($settings , 'logo_width') ?>" />
					</a>
					
				</div>
				<div id="navigation" class="navbar-collapse collapse">
				<?php 
					if(!(sh_set($settings , 'cs_one_page_settings'))):
						wp_nav_menu(
								array(  
									'theme_location'=> 'main_menu', 
									'menu_class'=>'nav navbar-nav navbar-right' ,
									'menu_id' => '' , 
									'container' => false, 
									'depth' => 1,
								)
						); 
					endif;
					
					if(sh_set($settings , 'cs_one_page_settings')):
				?>
					<!-- NAVIGATION LINKS -->
					<ul class="nav navbar-nav navbar-right">
					<?php 
						
						if((is_home() || is_front_page()) && !($wp_query->is_posts_page)):
							foreach($home_sections as $home_section): 
							if(sh_set(sh_set($home_section, 'section_page'), 0) == 'app_download') $id = 'home' ;
							elseif(sh_set(sh_set($home_section, 'section_page'), 0) == 'close_up_features') $id = 'features';
							elseif(sh_set(sh_set($home_section, 'section_page'), 0) == 'features_with_icons') $id = 'features-icons';
							elseif(sh_set(sh_set($home_section, 'section_page'), 0) == 'video_section') $id = 'video';
							elseif(sh_set(sh_set($home_section, 'section_page'), 0) == 'long_description') $id = 'description';
							elseif(sh_set(sh_set($home_section, 'section_page'), 0) == 'testimonials') $id = 'testimonials';
							elseif(sh_set(sh_set($home_section, 'section_page'), 0) == 'price_tables') $id = 'pricing';
							elseif(sh_set(sh_set($home_section, 'section_page'), 0) == 'screenshots_slider') $id = 'screenshots';
							elseif(sh_set(sh_set($home_section, 'section_page'), 0) == 'logo_section') $id = 'press';
							elseif(sh_set(sh_set($home_section, 'section_page'), 0) == 'newsletter') $id = 'subscribe';
							if(sh_set($home_section , 'tocopy')) break;
					?>
							<li>
								<a href="#<?php echo esc_html($id) ; ?>">
									<?php echo sh_set($home_section,'menu_name'); ?>
								</a>
							</li>
						<?php
								
							endforeach;
							foreach((array)$one_page_section_links as $one_page_section_link): 
							if(sh_set($one_page_section_link , 'tocopy')) break;
						?>
							<li>
								<a href="<?php echo sh_set($one_page_section_link,'menu_page_link'); ?>">
									<?php echo sh_set($one_page_section_link,'menu_name_link'); ?>
								</a>
							</li>
					<?php
							endforeach;
						endif;
					?>
					<?php if(!(is_home() || is_front_page()) || ($wp_query->is_posts_page)): ?>
						<li>
							<a href="<?php echo home_url();?>"><?php _e("Back To Home" , SH_NAME);?></a>
						</li>
					<?php endif ; ?>
						
					</ul>

				</div>
				<?php endif; ?>
			</div>
		</header>
