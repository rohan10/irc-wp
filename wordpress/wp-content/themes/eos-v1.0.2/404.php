<?php 
	get_header(); 
	$options = get_option(SH_NAME.'_theme_options');
?>
<!--
=================================
TITLE SECTION
=================================
-->
<section id="title" class="title-section section-inverse-color section parallax-background" data-stellar-background-ratio="0.4" style="background-image: url(<?php echo sh_set($options, 'blog_bg_image'); ?>); background-repeat: no-repeat; background-size: cover;">

	<!-- BACKGROUND OVERLAY -->
	<div class="black-background-overlay"></div>

	<div class="container">

		<h1 class="title"><?php echo wp_title(''); ?></h1>

	</div>
</section>

<!--
=================================
CONTENT SECTION
=================================
-->
<section id="content" class="content-section section">

	<div class="container">

		<div class="row">
			<div class="col-md-10 col-md-offset-1">

				<div class="blog-index">
				
					<article id="post-1" class="post-1 post">
						
						<h1 class="post-title1"><?php _e("404",SH_NAME); ?></h1>
            
						<h2 class="post-title3"><?php _e("Ups, PAGE NOT FOUND",SH_NAME); ?></h2>
						
						<h5 class="post-title2"><?php _e("We are sorry, but the page<br>you were looking for doesnot exist.",SH_NAME); ?></h5>
												
					</article>
	
				</div>

			</div>
		</div>

	</div>
</section>
<?php get_footer();?>
