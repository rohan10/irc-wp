<?php 
	get_header();
	$options = get_option(SH_NAME.'_theme_options');
?>

<div class="navbar border-bottom">
    <div class="container">
        <?php if(sh_set($options , 'logo_image')): ?>
        <div class="navbar-header"> 
			<a class="navbar-brand" href="<?php echo esc_url(home_url('/')) ; ?>">
				<img alt="" src="<?php echo sh_set($options , 'logo_image'); ?>" />
			</a> 
		</div>
		<?php endif; ?>
         <?php 
	   if(sh_set($options , 'onepage_set') == '1'){ ?>
       <a href="<?php home_url('/'); ?>" class="back-btn"><?php _e("Back to Home" , SH_NAME); ?><i class="fa fa-share"></i></a>
      
       <?php } else { ?>
          
          <?php wp_nav_menu(array('theme_location' => 'main_menu' , 'container' => 'div' , 'container_class'=> 'collapse navbar-collapse' ,'menu_class' => 'nav navbar-nav' , 'depth'=>1));  ?>
       
       <?php } ?>
    </div>
</div>


<article id="blog" class="section">
    <div class="heading-upper">
      <div class="container">
        <header class="heading animated out" data-animation="fadeInUp" data-delay="0">
        <h2><?php echo get_search_query(); ?></h2>
          
        </header>
      </div>
    </div>
    <div class="content-section no-padding-bottom">
      <div class="container">
        <div class="blog-area blog-single-page">
          <div class="row">
          	<div class="col-md-12">
            	<div class="row">
                	<?php if(have_posts()) : while(have_posts()) : the_post();  global $post ; ?> 
                    	<div data-delay="400" data-animation="fadeInUp" class="col-sm-4 animated fadeInUp in">
                      <div class="blog-box">
                        <figure class="image"> <?php the_post_thumbnail('370x267'); ?> </figure>
                        <div class="detail">
                          <h4><?php the_title(); ?></h4>
                          <p class="meta"><?php _e("Posted by " , SH_NAME); ?> <?php the_author_posts_link(); ?> - <?php echo get_the_date('Y-m-d'); ?> at <?php echo get_the_time(); ?>  </p>
                          <p><?php echo substr(strip_tags(get_the_content()) , 0 , 120); ?></p>
                          <a class="btn btn-gray btn-block blog-popup" href="<?php the_permalink(); ?>"><?php _e("READ MORE" , SH_NAME); ?> <i class="icon-external-link"></i></a> </div>
                      </div>
                    </div>
                    <?php	endwhile ; endif; 
					 _the_pagination();
					 wp_reset_query(); ?> 
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </article>
<?php get_footer();?>