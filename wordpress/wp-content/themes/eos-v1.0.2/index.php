<?php 
	get_header(); 
	$options = get_option(SH_NAME.'_theme_options');
	$anchor_color = (sh_set($options , 'default_color_scheme') == 'light') ? '444444' : 'FFF' ;
?>
<!--
=================================
TITLE SECTION
=================================
-->
<section id="title" class="title-section section-inverse-color section parallax-background" data-stellar-background-ratio="0.4" style="background-image: url(<?php echo sh_set($options, 'blog_bg_image'); ?>); background-repeat: no-repeat; background-size: cover;">

	<!-- BACKGROUND OVERLAY -->
	<div class="black-background-overlay"></div>

	<div class="container">

		<h1 class="title">
			<?php echo (sh_set($options , 'blog_header_title')) ? sh_set($options , 'blog_header_title') :__("Blog",SH_NAME); ?>
		</h1>

	</div>
</section>

<!--
=================================
CONTENT SECTION
=================================
-->
<section id="content" class="content-section section">

	<div class="container">

		<div class="row">
			<div class="col-md-10 col-md-offset-1">

				<div class="blog-index">
				
					<?php
						if(have_posts()):  while(have_posts()): the_post(); 
						global $post ;
						$meta = get_post_meta(get_the_ID(), 'sh_post_meta', true);
						$author = sh_set(sh_set(sh_set($meta , 'sh_post_general') , 0 ), 'author');
					?>

					<article id="post-1"  <?php post_class('post-1 post '); ?>>

						<h2 class="post-title">
							<a href="<?php the_permalink(); ?>" style="font-size: 40px; line-height: 50px; font-weight: 300; color:#<?php echo esc_html($anchor_color); ?>;">
								<?php echo get_the_title(); ?>
							</a>
						</h2>
						<div class="post-meta">
							<span class="post-meta-item post-meta-date"><?php the_time('F j, Y') ?></span>
							<?php if($author): ?>
								<span class="post-meta-item post-meta-author"><?php echo esc_html($author); ?></span>
							<?php endif;?>
							<span class="post-meta-item post-meta-category">
							<?php 
								$categories = get_the_category();
								$i = 0;
								foreach((array)$categories as $cat) {
									if ($i == 0):
										echo '<a href="'.get_category_link($cat->cat_ID).'"> ' . $cat->cat_name . '</a>';
										break;
									endif;
									$i++;
								}
								if(has_tag()):
							?>, 
							<?php
									$tags = get_the_tags();
									if(has_tag()):
										$j = 0;
										foreach((array)$tags as $tag) {
											if ($j == 0):
												echo '<a href="'.get_tag_link($tag->term_id).'"> ' . $tag->name . '</a>';
												break;
											endif;
											$j++;
										}
									endif;
								endif;
							?>
							</span>
						</div>
						<?php if(has_post_thumbnail()): ?>
						<div class="post-thumbnail">
							<?php echo the_post_thumbnail('945x500'); ?>
						</div>
						<?php endif; ?>
						<div class="post-content">
							<?php echo the_excerpt(); ?>
							<p><a href="<?php the_permalink(); ?>" class="read-more"><?php _e("Continue Reading..." , SH_NAME);?></a></p>
						</div>

					</article>
					
					<?php
						endwhile ; endif ;
						wp_reset_query();
					?>
	
				</div>

				<div class="blog-pagination">
					<span class="blog-pagination-link-left"><?php previous_posts_link( 'Older Posts' ); ?></span>
					<span class="blog-pagination-link-right"><?php next_posts_link( 'Newer Posts' ); ?></span>
				</div>

			</div>
		</div>

	</div>
</section>
<?php get_footer();?>