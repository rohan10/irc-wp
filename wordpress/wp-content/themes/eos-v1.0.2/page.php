<?php 
	get_header();
	$meta = get_post_meta(get_the_ID() , 'sh_post_meta' , true); 
	$author = sh_set(sh_set(sh_set($meta , 'sh_post_general') , 0 ), 'author');
	$bg_image = sh_set(sh_set(sh_set($meta , 'sh_post_general') , 0 ), 'bg');
	$header_title = sh_set(sh_set(sh_set($meta , 'sh_post_general') , 0) , 'header_text') ;
	if($header_title):
?>
<!--
=================================
TITLE SECTION
=================================
-->
<section id="title" class="title-section section-inverse-color section parallax-background" data-stellar-background-ratio="0.4" style="background-image: url(<?php echo esc_url($bg_image); ?>); background-repeat: no-repeat; background-size: cover;">

	<!-- BACKGROUND OVERLAY -->
	<div class="black-background-overlay"></div>

	<div class="container">

		<h1 class="title"><?php echo esc_html($header_title); ?></h1>

	</div>
</section>
<?php endif; ?>
<!--
=================================
CONTENT SECTION
=================================
-->
<section id="content" class="content-section section">

	<div class="container">
	<?php 
		while(have_posts()): the_post(); 
		$tags = get_the_tags();
	  ?>
		<div class="row">
			<div class="col-md-10 col-md-offset-1">

				<div class="blog-single">

					<article id="post-1" class="post-1 post">

						<h2 class="post-title"><?php echo get_the_title(); ?></h2>
						<div class="post-meta">
							<span class="post-meta-item post-meta-date"><?php the_time('F j, Y') ?></span>
							<?php if($author): ?>
								<span class="post-meta-item post-meta-author"><?php echo esc_html($author); ?></span>
							<?php endif;?>
							
						</div>

						<?php if(has_post_thumbnail()): ?>
							<div class="post-thumbnail">
								<?php echo the_post_thumbnail('945x500'); ?>
							</div>
						<?php endif; ?>

						<div class="post-content">
							<?php echo the_content(); ?>
						</div>

					</article>

				</div>

				<?php comments_template(); ?>
				<?php wp_link_pages(); ?>

			</div>
		</div>
	<?php endwhile; ?>
	</div>
</section>

<?php get_footer(); ?>