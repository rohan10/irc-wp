<?php
return array(
     'title' => __( 'EOS Theme Options', SH_NAME ),
    'logo' => get_template_directory_uri() . '/images/logo.png',
    'menus' => array(

        // General Settings
         array(
             'title' => __( 'General Settings', SH_NAME ),
            'name' => 'general_settings',
            'icon' => 'font-awesome:icon-cogs',
            'menus' => array(
                 
				 array(
                     'title' => __( 'General Settings', SH_NAME ),
                    'name' => 'general_settings',
                    'icon' => 'font-awesome:icon-cog',
                    'controls' => array(
                         array(
                            'type' => 'section',
                            'repeating' => true,
                            'sortable' => true,
                            'title' => __( 'Color Scheme', SH_NAME ),
                            'name' => 'color_schemes',
                            'description' => __( 'This section is used for theme color settings', SH_NAME ),
                            'fields' => array(
                                
                                array(
                                    'type' => 'radioimage',
                                    'name' => 'custom_color_scheme',
                                    'label' => __( 'Color Scheme', SH_NAME ),
                                    'description' => __( 'Choose the Custom color scheme for the theme.', SH_NAME ),
									'items' => array(
										 array(
											'value' => 'green',
											'label' => __('Green', SH_NAME),
											'img' => get_template_directory_uri().'/images/green.png' ,
											),
											array(
											'value' => 'turquoise',
											'label' => __('Turquoise', SH_NAME),
											'img' => get_template_directory_uri().'/images/turquoise.png' ,
											),
											array(
											'value' => 'blue',
											'label' => __('Blue', SH_NAME),
											'img' => get_template_directory_uri().'/images/blue.png' ,
											),
											array(
											'value' => 'purple',
											'label' => __('Purple', SH_NAME),
											'img' => get_template_directory_uri().'/images/purple.png' ,
											),
											array(
											'value' => 'pink',
											'label' => __('Pink', SH_NAME),
											'img' => get_template_directory_uri().'/images/pink.png' ,
											),
											array(
											'value' => 'red',
											'label' => __('Red', SH_NAME),
											'img' => get_template_directory_uri().'/images/red.png' ,
											),
											array(
											'value' => 'orange',
											'label' => __('Orange', SH_NAME),
											'img' => get_template_directory_uri().'/images/orange.png' ,
											),
											array(
											'value' => 'yellow',
											'label' => __('Yellow', SH_NAME),
											'img' => get_template_directory_uri().'/images/yellow.png' ,
											),
									), 
                                ),
                               
                                
                                array(
                                    'type' => 'radioimage',
                                    'name' => 'default_color_scheme',
                                    'label' => __( 'Website Theme', SH_NAME ),
                                    'description' => __('Choose Website Theme Either to be Light or Dark', SH_NAME ),
                                    'deafault' => 'light',
                                    'items' => 
									array(
                                        array(
                                            'value' => 'light',
                                            'label' => __('Light' , SH_NAME) ,
											'img' => get_template_directory_uri().'/images/light.png' ,
                                        ),
                                        array(
                                            'value' => 'dark',
                                            'label' => __('Dark' , SH_NAME) ,
											'img' => get_template_directory_uri().'/images/dark.png' ,
                                        ) 
                                    ) 
                                ),
								                                
                            ) 
                        ),
						 
						 array(
							'type' => 'textbox',
							'name' => 'app_store_link',
							'label' => __('App Store Link', SH_NAME),
							'description' => __('Enter Link to Download App from APP Store.', SH_NAME),
							
						      ),
						
						 array(
							'type' => 'textbox',
							'name' => 'play_store_link',
							'label' => __('Play Store Link', SH_NAME),
							'description' => __('Enter Link to Download App from Play Store.', SH_NAME),
							
						    ),
						array(
							'type' => 'textbox',
							'name' => 'win_store_link',
							'label' => __('Windows Store Link', SH_NAME),
							'description' => __('Enter Link to Download App from Windows Store.', SH_NAME),
							
						    ),
						array(
							'type' => 'builder',
							'label' => __('Link Images', SH_NAME ),
							'name' => 'link_images',
							'description' => __( 'Upload Images and add their Links.', SH_NAME ),	
							'fields' => array(
											array(
												'type' => 'upload',
												'name' => 'link_uploaded_image',
												'label' => __('Image', SH_NAME),
												'description' => __('Upload Image for Link.', SH_NAME),
											),
											array(
												'type' => 'textbox',
												'name' => 'uploaded_image_link',
												'label' => __('Image Link', SH_NAME),
												'description' => __('Enter Link for Image.', SH_NAME),
											),
								
								),
						),
                    ) 
                ),
				
				/** Preloader Settings */
                array(
                     'title' => __( 'Preloader Settings', SH_NAME ),
                    'name' => 'sub_preloader_settings',
                    'icon' => 'font-awesome:icon-refresh',
                    'controls' => array(
						array(
							'type' => 'toggle',
							'name' => 'cs_preloader',
							'label' => __('Preloader ?', SH_NAME),
							'description' => __('Check this if you want to Preloader', SH_NAME),
							),
						array(
                            'type' => 'upload',
                            'name' => 'cs_preloader_image',
							'label' => __('Image', SH_NAME),
							'description' => __('Upload Image for Preloader.', SH_NAME),
							'dependency' => array(
												'field' => 'cs_preloader',
												'function' => 'vp_dep_boolean' 
											)
						),
						array(
							'type' => 'textarea',
							'name' => 'cs_preloader_text',
							'label' => __('Text', SH_NAME),
							'description' => __('Enter Text for Preloader', SH_NAME),
							'dependency' => array(
												'field' => 'cs_preloader',
												'function' => 'vp_dep_boolean' 
											)
						),
                                                
                    ) 
                ),
                
				/** Submenu for heading settings */
                array(
                     'title' => __( 'Header Settings', SH_NAME ),
                    'name' => 'header_settings',
                    'icon' => 'font-awesome:icon-credit-card',
                    'controls' => array(
                         array(
                             'type' => 'upload',
                            'name' => 'site_favicon',
                            'label' => __( 'Favicon', SH_NAME ),
                            'description' => __( 'Upload the favicon, should be 16x16', SH_NAME ),
                            'default' => '' 
                        ),
                        array(
							'type' => 'toggle',
							'name' => 'header_float',
							'label' => __('Floating Header ?', SH_NAME),
							'description' => __('Turn this on if you want your header to appear on scroll.', SH_NAME),
							),
                        array(
                             'type' => 'section',
                            'title' => __( 'Logo with Image', SH_NAME ),
                            'name' => 'logo_with_image',
                            
                            'fields' => array(
                                 array(
                                     'type' => 'upload',
                                    'name' => 'logo_image',
                                    'label' => __( 'Logo Image', SH_NAME ),
                                    'description' => __( 'Upload the logo image', SH_NAME ),
                                    'default' => '',
                                ),
                                array(
                                     'type' => 'slider',
                                    'name' => 'logo_width',
                                    'label' => __( 'Logo Width', SH_NAME ),
                                    'description' => __( 'choose the logo width', SH_NAME ),
                                    'default' => '144',
                                    'min' => 20,
                                    'max' => 400 
                                ),
                                array(
                                     'type' => 'slider',
                                    'name' => 'logo_height',
                                    'label' => __( 'Logo Height', SH_NAME ),
                                    'description' => __( 'choose the logo height', SH_NAME ),
                                    'default' => '45',
                                    'min' => 20,
                                    'max' => 400 
                                ) 
                                
                            ) 
                        ),
                        
                        // Custom HEader Style End
                        array(
                             'type' => 'codeeditor',
                            'name' => 'header_css',
                            'label' => __( 'Header CSS', SH_NAME ),
                            'description' => __( 'Write your custom css to include in header.', SH_NAME ),
                            'theme' => 'github',
                            'mode' => 'css' 
                        ) 
                    ) 
                    
                ),
                
                /** Submenu for footer area */
                array(
                     'title' => __( 'Footer Settings', SH_NAME ),
                    'name' => 'sub_footer_settings',
                    'icon' => 'font-awesome:icon-hdd',
                    'controls' => array(
					
						array(
							'type' => 'toggle',
							'name' => 'cs_form_include',
							'label' => __('Contact Form ?', SH_NAME),
							'description' => __('Check this if you want to include Contact Form .', SH_NAME),							          
						),
						
						array(
							'type' => 'textbox',
							'name' => 'cs_contant_title',
							'label' => __('Title', SH_NAME),
							'description' => __('Enter Title for Contact section.', SH_NAME),
							'dependency' => array(
												'field' => 'cs_form_include',
												'function' => 'vp_dep_boolean' 
											)
						),
						array(
							'type' => 'textarea',
							'name' => 'cs_contant_description',
							'label' => __('Description', SH_NAME),
							'description' => __('Enter Description for Contact section.', SH_NAME),
							'dependency' => array(
												'field' => 'cs_form_include',
												'function' => 'vp_dep_boolean' 
											)
						),
						array(
							'type' => 'textbox',
							'name' => 'cs_contant_button_text',
							'label' => __('Button Text', SH_NAME),
							'description' => __('Enter Button Text for Contact Form.', SH_NAME),
							'dependency' => array(
												'field' => 'cs_form_include',
												'function' => 'vp_dep_boolean' 
											)
						),			
						array(
							'type' => 'toggle',
							'name' => 'cs_app_store_links',
							'label' => __('App Store Link', SH_NAME),
							'description' => __('Check if you want to include the App Store Link to this Section .', SH_NAME),				
						),
						array(
							'type' => 'toggle',
							'name' => 'cs_play_store_links',
							'label' => __('Play Store Link', SH_NAME),
							'description' => __('Check if you want to include the Play Store Link to this Section .', SH_NAME),				
						),
						array(
							'type' => 'toggle',
							'name' => 'cs_win_store_links',
							'label' => __('Windows Store Link', SH_NAME),
							'description' => __('Check if you want to include the Windows Store Link to this Section .', SH_NAME),				
						),
						array(
							'type' => 'textbox',
							'name' => 'link_images_footer',
							'label' => __('Number Of Link Images', SH_NAME),
							'description' => __('Enter Numbers of Link Images to show.', SH_NAME),
						),	  
						array(
							'type' => 'builder',
							'label' => __('Social Media', SH_NAME ),
							'name' => 'add_social_media',
							'description' => __( 'ADD Social Media Links in Contact Us Section.', SH_NAME ),	
							'fields' => array(
								
											array(
												'type' => 'textbox',
												'name' => 'cs_sm_link',
												'label' => __('Social Media Link', SH_NAME),
												'description' => __('Enter Social Media Link.', SH_NAME),
											),
											 array(
													'type' => 'select',
													'name' => 'cs_social_media_icon',
													'label' => __('Social Media Icon', SH_NAME),
													'description' => __('Choose Social Media Icon.', SH_NAME),
													'items' => array(
																'data' => array(
																			array(
																				'source' => 'function',
																				'value' => 'vp_get_social_medias',
																			),
																		  ),
																),
											),
								
								),
						),
					
                        array(
                             'type' => 'textarea',
                            'name' => 'copyright_text',
                            'label' => __( 'Copyright Text', SH_NAME ),
                            'description' => __( 'Enter the Copyright Text', SH_NAME ),
                            'default' => '' 
                        ),
                        array(
                             'type' => 'codeeditor',
                            'name' => 'footer_analytics',
                            'label' => __( 'Footer Analytics / Scripts', SH_NAME ),
                            'description' => __( 'In this area you can put Google Analytics Code or any other Script that you want to be included in the footer before the Body tag.', SH_NAME ),
                            'theme' => 'twilight',
                            'mode' => 'javascript' 
                        ) 
                                                
                    ) 
                ), 
				                
                
            ) 
        ),
		// Build Home Page Options
		array(
            'title' => __( 'Build Home Page', SH_NAME ),
            'name' => 'build_home_page',
            'icon' => 'font-awesome:icon-wrench',
            'menus' => array(
                 
				array(
			'title' => __('Home Page Settings', SH_NAME),
			'name' => 'homepage_settings',
			'icon' => 'font-awesome:icon-magic',
			'controls' => array(
						array(
							'type' => 'toggle',
							'name' => 'cs_one_page_settings',
							'label' => __('One Page ?', SH_NAME),
							'description' => __('Check this if you want to make yout theme a onepage theme.', SH_NAME),				
						),
						array(
							'type' => 'builder',
							'repeating' => true,
							'sortable'  => true,
							'label' => __('Section', SH_NAME),
							'name' => 'one_page_section',
							'description' => __('This section is used to add a section to your Home Page', SH_NAME),
							'fields' => array(
								array(
									'type' => 'textbox',
									'name' => 'menu_name',
									'label' => __('Menu Name', SH_NAME),
									'description' => __('Menu Name', SH_NAME),
								),
								array(
                                    'type' => 'multiselect',
                                    'name' => 'section_page',
                                    'label' => __( 'Choose Section', SH_NAME ),
                                    'description' => __( 'Choose sections To integrate with Menu Name. You Can choose Multiple Sections well', SH_NAME ),
                                    'items' => array(
										 array(
											'value' => 'app_download',
											'label' => __('App Download Section', SH_NAME),
											),
											array(
											'value' => 'close_up_features',
											'label' => __('Close Up Features', SH_NAME),
											),
											array(
											'value' => 'features_with_icons',
											'label' => __('Features With Icons', SH_NAME),
											),
											array(
											'value' => 'video_section',
											'label' => __('Video Section', SH_NAME),
											),
											array(
											'value' => 'long_description',
											'label' => __('Long Description', SH_NAME),
											),
											array(
											'value' => 'testimonials',
											'label' => __('Testimonials Sections', SH_NAME),
											),
											array(
											'value' => 'price_tables',
											'label' => __('Packages Section', SH_NAME),
											),
											array(
											'value' => 'screenshots_slider',
											'label' => __('Screenshots Slider', SH_NAME),
											),
											array(
											'value' => 'logo_section',
											'label' => __('Logos Section', SH_NAME),
											),
											array(
											'value' => 'newsletter',
											'label' => __('NewsLetter FeedBurner', SH_NAME),
											),
									),
                                ),
							),
						),
						array(
							'type' => 'builder',
							'repeating' => true,
							'sortable'  => true,
							'label' => __('Links', SH_NAME),
							'name' => 'one_page_section_links',
							'description' => __('This section is used to add a menu with link to your Home Page', SH_NAME),
							'fields' => array(
								array(
									'type' => 'textbox',
									'name' => 'menu_name_link',
									'label' => __('Menu Name', SH_NAME),
									'description' => __('Menu Name', SH_NAME),
								),
								array(
									'type' => 'textbox',
									'name' => 'menu_page_link',
									'label' => __('Menu Link', SH_NAME),
									'description' => __('Menu Link', SH_NAME),
								),
							),
						),
						
						
					),
				),
				array(
					'title' => __( 'Hero Section', SH_NAME ),
					'name' => 'app_download_settings',
					'icon' => 'font-awesome:icon-download',
					'controls' => array(
							 array(
							 'type' => 'section',
							'title' => __( 'Hero Section', SH_NAME ),
							'name' => 'app_download_section',
							'fields' => array(
								
								 array(
									'type' => 'radioimage',
									'name' => 'theme_layouts',
									'label' => __( 'Layout', SH_NAME ),
									'description' => __( 'Choose Layout.', SH_NAME ),														                                            'deafault' => 'layout_1',
									'items' => array(
										 array(
											 'value' => 'layout_1',
											'label' => __('Layout 1' , SH_NAME),
											'img' => get_template_directory_uri().'/images/layout1.png' , 
											
										),
										array(
											 'value' => 'layout_2',
											'label' => __('Layout 2' , SH_NAME) ,
											'img' => get_template_directory_uri().'/images/layout2.png' ,
										) ,
										array(
											 'value' => 'layout_3',
											'label' => __('Layout 3' , SH_NAME) ,
											'img' => get_template_directory_uri().'/images/layout3.png' ,
										),
										array(
											 'value' => 'layout_4',
											'label' => __('Layout 4' , SH_NAME) ,
											'img' => get_template_directory_uri().'/images/layout4.png' ,
										),
									) 
								),
								
								
								// Layout 1 Dependent Fileds
								 array(
										'type' => 'upload',
										'name' => 'app_logo_l1',
										'label' => __( 'App Logo', SH_NAME ),
										'description' => __( 'Upload the App Logo', SH_NAME ),
										'default' => '' ,
										'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout1' 
												)
									),
								 array(
									'type' => 'upload',
									'name' => 'app_left_image_l1',
									'label' => __( 'App Image', SH_NAME ),
									'description' => __( 'Upload Image to be used as Left Side Image.', SH_NAME ),
									'default' => '',
									'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout1' 
												)
								),
								 array(
									'type' => 'textbox',
									'name' => 'app_download_heading_l1',
									'label' => __('Heading', SH_NAME),
									'description' => __('Enter text to be displayed as App Heading.', SH_NAME),
									'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout1' 
												),
									
								),
								 array(
									'type' => 'textarea',
									'name' => 'app_download_description_l1',
									'label' => __('Description', SH_NAME),
									'description' => __('Enter text to be displayed as APP Description.', SH_NAME),
									'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout1' 
												),
								),
								array(
									'type' => 'textbox',
									'name' => 'link_images_l1',
									'label' => __('Number Of Link Images', SH_NAME),
									'description' => __('Enter Numbers of Link Images to show.', SH_NAME),
									'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout1' 
												),
									
								),
								array(
									'type' => 'toggle',
									'name' => 'app_store_links_l1',
									'label' => __('App Store Link', SH_NAME),
									'description' => __('Check if you want to include the App Store Link to this Section .', SH_NAME),				
									'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout1' 
												)
								),
								array(
									'type' => 'toggle',
									'name' => 'play_store_links_l1',
									'label' => __('Play Store Link', SH_NAME),
									'description' => __('Check if you want to include the Play Store Link to this Section .', SH_NAME),				
									'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout1' 
												)
								),
								array(
									'type' => 'toggle',
									'name' => 'win_store_links_l1',
									'label' => __('Windows Store Link', SH_NAME),
									'description' => __('Check if you want to include the Windows Store Link to this Section .', SH_NAME),				
									'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout1' 
												)
								),
								
								// Layout 2 Dependent Fields
								 array(
										'type' => 'upload',
										'name' => 'app_logo_l2',
										'label' => __( 'App Logo', SH_NAME ),
										'description' => __( 'Upload the App Logo', SH_NAME ),
										'default' => '' ,
										'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout2' 
												)
									),
								 array(
									'type' => 'textbox',
									'name' => 'app_download_heading_l2',
									'label' => __('Heading', SH_NAME),
									'description' => __('Enter text to be displayed as App Heading.', SH_NAME),
									'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout2' 
												),
									
								),
								array(
									'type' => 'textbox',
									'name' => 'link_images_l2',
									'label' => __('Number Of Link Images', SH_NAME),
									'description' => __('Enter Numbers of Link Images to show.', SH_NAME),
									'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout2' 
												),
									
								),
								array(
									'type' => 'toggle',
									'name' => 'app_store_links_l2',
									'label' => __('App Store Link', SH_NAME),
									'description' => __('Check if you want to include the App Store Link to this Section .', SH_NAME),				
									'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout2' 
												)
								),
								array(
									'type' => 'toggle',
									'name' => 'play_store_links_l2',
									'label' => __('Play Store Link', SH_NAME),
									'description' => __('Check if you want to include the Play Store Link to this Section .', SH_NAME),				
									'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout2' 
												)
								),
								array(
									'type' => 'toggle',
									'name' => 'win_store_links_l2',
									'label' => __('Windows Store Link', SH_NAME),
									'description' => __('Check if you want to include the Windows Store Link to this Section .', SH_NAME),				
									'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout2' 
												)
								),									
								
								// Layout 3 Dependent Fields
								 array(
										'type' => 'upload',
										'name' => 'app_logo_l3',
										'label' => __( 'App Logo', SH_NAME ),
										'description' => __( 'Upload the App Logo', SH_NAME ),
										'default' => '' ,
										'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout3' 
												)
									),
								 array(
									'type' => 'upload',
									'name' => 'app_left_image1_l3',
									'label' => __( 'App Image', SH_NAME ),
									'description' => __( 'Upload Image to be used as Left Side Image.', SH_NAME ),
									'default' => '',
									'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout3' 
												)
								),
								 array(
									'type' => 'upload',
									'name' => 'app_left_image2_l3',
									'label' => __( 'App Image', SH_NAME ),
									'description' => __( 'Upload Image to be used as Left Side Image.', SH_NAME ),
									'default' => '',
									'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout3' 
												)
								),
								 array(
									'type' => 'textbox',
									'name' => 'app_download_heading_l3',
									'label' => __('Heading', SH_NAME),
									'description' => __('Enter text to be displayed as App Heading.', SH_NAME),
									'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout3' 
												),
									
								),
								 array(
									'type' => 'textarea',
									'name' => 'app_download_description_l3',
									'label' => __('Description', SH_NAME),
									'description' => __('Enter text to be displayed as APP Description.', SH_NAME),
									'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout3' 
												),
								),
								array(
									'type' => 'textbox',
									'name' => 'link_images_l3',
									'label' => __('Number Of Link Images', SH_NAME),
									'description' => __('Enter Numbers of Link Images to show.', SH_NAME),
									'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout3' 
												),
									
								),
								array(
									'type' => 'toggle',
									'name' => 'app_store_links_l3',
									'label' => __('App Store Link', SH_NAME),
									'description' => __('Check if you want to include the App Store Link to this Section .', SH_NAME),				
									'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout3' 
												)
								),
								array(
									'type' => 'toggle',
									'name' => 'play_store_links_l3',
									'label' => __('Play Store Link', SH_NAME),
									'description' => __('Check if you want to include the Play Store Link to this Section .', SH_NAME),				
									'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout3' 
												)
								),
								array(
									'type' => 'toggle',
									'name' => 'win_store_links_l3',
									'label' => __('Windows Store Link', SH_NAME),
									'description' => __('Check if you want to include the Windows Store Link to this Section .', SH_NAME),				
									'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout3' 
												)
								),	
								
								
								// Layout 4 Dependent Fileds
								 array(
										'type' => 'upload',
										'name' => 'app_logo_l4',
										'label' => __( 'App Logo', SH_NAME ),
										'description' => __( 'Upload the App Logo', SH_NAME ),
										'default' => '' ,
										'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout4' 
												)
									),
								 array(
									'type' => 'upload',
									'name' => 'app_left_image_l4',
									'label' => __( 'App Image', SH_NAME ),
									'description' => __( 'Upload Image to be used as Left Side Image.', SH_NAME ),
									'default' => '',
									'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout4' 
												)
								),
								 array(
									'type' => 'textbox',
									'name' => 'app_download_heading_l4',
									'label' => __('Heading', SH_NAME),
									'description' => __('Enter text to be displayed as App Heading.', SH_NAME),
									'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout4' 
												),
									
								),
								 array(
									'type' => 'textarea',
									'name' => 'app_download_description_l4',
									'label' => __('Description', SH_NAME),
									'description' => __('Enter text to be displayed as APP Description.', SH_NAME),
									'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout4' 
												),
								),
								array(
									'type' => 'textbox',
									'name' => 'link_images_l4',
									'label' => __('Number Of Link Images', SH_NAME),
									'description' => __('Enter Numbers of Link Images to show.', SH_NAME),
									'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout4' 
												),
									
								),
								array(
									'type' => 'toggle',
									'name' => 'app_store_links_l4',
									'label' => __('App Store Link', SH_NAME),
									'description' => __('Check if you want to include the App Store Link to this Section .', SH_NAME),				
									'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout4' 
												)
								),
								array(
									'type' => 'toggle',
									'name' => 'play_store_links_l4',
									'label' => __('Play Store Link', SH_NAME),
									'description' => __('Check if you want to include the Play Store Link to this Section .', SH_NAME),				
									'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout4' 
												)
								),
								array(
									'type' => 'toggle',
									'name' => 'win_store_links_l4',
									'label' => __('Windows Store Link', SH_NAME),
									'description' => __('Check if you want to include the Windows Store Link to this Section .', SH_NAME),				
									'dependency' => array(
													'field' => 'theme_layouts',
													'function' => 'vp_dep_is_layout4' 
												)
								),
								
								// image and video dependency
								
								array(
									'type' => 'radioimage',
									'name' => 'section_bg_option',
									'label' => __( 'Section Background', SH_NAME ),
									'description' => __( 'Choose Background Option.', SH_NAME ),														                                            'deafault' => '',
									'items' => array(
										 array(
											 'value' => 'image',
											'label' => __('Image' , SH_NAME),
											'img' => get_template_directory_uri().'/images/image.png' , 
											
										),
										array(
											 'value' => 'video',
											'label' => __('Video' , SH_NAME) ,
											'img' => get_template_directory_uri().'/images/video.png' ,
										) ,
									) 
								),
								
								// Image BG Dependent
								array(
									'type' => 'upload',
									'name' => 'app_download_section_bg',
									'label' => __( 'Section Background Image', SH_NAME ),
									'description' => __( 'Upload Image to be used as Section Background.', SH_NAME ),
									'default' => '',
									'dependency' => array(
													'field' => 'section_bg_option',
													'function' => 'vp_dep_is_bg_image' 
												)
								),
								
								
								// Video BG Dependent 
								
								array(
									'type' => 'upload',
									'name' => 'app_download_section_video_bg',
									'label' => __( 'Section Background Video', SH_NAME ),
									'description' => __( 'Upload Video to be used as Section Background (.mp4 only).', SH_NAME ),
									'default' => '',
									'dependency' => array(
													'field' => 'section_bg_option',
													'function' => 'vp_dep_is_bg_video' 
												)
								),
								
								
								
								
																
							) 
							),
						
					   
					) 
			
                ),
				array(
							'title' => __( 'Close Up Features', SH_NAME ),
							'name' => 'close_up_features_settings',
							'icon' => 'font-awesome:icon-search',
							'controls' => array(
									array(
											'type' => 'textbox',
											'name' => 'close_up_features_heading',
											'label' => __('Heading', SH_NAME),
											'description' => __('Enter text to be displayed as Heading of this Section.', SH_NAME),											
										),
								   array(
											'type' => 'textbox',
											'name' => 'close_up_features_tagline',
											'label' => __('Tag Line', SH_NAME),
											'description' => __('Enter text to be displayed as Tag line.', SH_NAME),
											
										),
								  array(
											'type' => 'upload',
											'name' => 'close_up_features_image',
											'label' => __( 'Features Image', SH_NAME ),
											'description' => __( 'Upload Image to be used as Features Centered Image.', SH_NAME ),
											'default' => '',
										),
									
								array(
									'type' => 'builder',
									'label' => __('Features ', SH_NAME ),
									'name' => 'close_up_features',
									'fields' => array(
										array(
											'type' => 'textbox',
											'name' => 'closeup_feature_title',
											'label' => __('Feature Title', SH_NAME),
											'description' => __('Enter text to be displayed as Feature Title.', SH_NAME),
										),
										array(
											'type' => 'textarea',
											'name' => 'closeup_feature_desc',
											'label' => __('Feature Description', SH_NAME),
											'description' => __('Enter text to be displayed as Feature Description.', SH_NAME											                                          ),
										),
									  ) 
									),							   
							) 
                ),
				array(
							'title' => __( 'Features with Icons', SH_NAME ),
							'name' => 'features_with_icons_settings',
							'icon' => 'font-awesome:icon-smile',
							'controls' => array(
									array(
											'type' => 'textbox',
											'name' => 'features_with_icons_heading',
											'label' => __('Heading', SH_NAME),
											'description' => __('Enter text to be displayed as Heading of this Section.', SH_NAME),											
										),
								   array(
											'type' => 'textbox',
											'name' => 'features_with_ions_tagline',
											'label' => __('Tag Line', SH_NAME),
											'description' => __('Enter text to be displayed as Tag line.', SH_NAME),
											
										),
								  array(
											'type' => 'upload',
											'name' => 'features_with_icons_image',
											'label' => __( 'Features Image', SH_NAME ),
											'description' => __( 'Upload Image to be used as Features Image.', SH_NAME ),
											'default' => '',
										),
									
								array(
									'type' => 'builder',
									'label' => __('Features ', SH_NAME ),
									'name' => 'features_with_icons',
									'fields' => array(
										array(
											'type' => 'textbox',
											'name' => 'feature_with_icon_title',
											'label' => __('Feature Title', SH_NAME),
											'description' => __('Enter text to be displayed as Feature Title.', SH_NAME),
										),
										array(
											'type' => 'select',
											'name' => 'feature_with_icon_icon',
											'label' => __( 'Feature Icon', SH_NAME ),
											'items' => array(
														 'data' => array(
																	 array(
																		'source' => 'function',
																		'value' => 'eos_theme_icons' 
																	 ) 
																   ) 
													   ),
											'description' => __( 'Choose Feature Icon.', SH_NAME ),
										),
										array(
											'type' => 'textarea',
											'name' => 'feature_with_icon_desc',
											'label' => __('Feature Description', SH_NAME),
											'description' => __('Enter text to be displayed as Feature Description.', SH_NAME											                                          ),
										),
									  ) 
									),							   
							) 
                ),
				array(
					'title' => __( 'Video Section', SH_NAME ),
					'name' => 'video_section_setings',
					'icon' => 'font-awesome:icon-facetime-video',
					'controls' => array(
						array(
							'type' => 'upload',
							'name' => 'video_section_bg',
							'label' => __( 'Video Section BG Image', SH_NAME ),
							'description' => __( 'Upload Image to be used as Background Image of this Section.', SH_NAME ),
							'default' => '',
						),	
						array(
							'type' => 'textbox',
							'name' => 'video_link',
							'label' => __('Video Link', SH_NAME),
							'description' => __('Enter Link of the Video to Show in this Section. (Vimeo & YouTube Links Only)', SH_NAME),
						),
					) 
                ),
				array(
							'title' => __( 'Long Description', SH_NAME ),
							'name' => 'long_desc_settings',
							'icon' => 'font-awesome:icon-align-left',
							'controls' => array(
									array(
											'type' => 'textbox',
											'name' => 'long_desc_heading',
											'label' => __('Heading', SH_NAME),
											'description' => __('Enter text to be displayed as Heading of this Section.', SH_NAME),											
										),
								  array(
											'type' => 'upload',
											'name' => 'long_desc_image1',
											'label' => __( 'Image 1', SH_NAME ),
											'description' => __( 'Upload Image.', SH_NAME ),
											'default' => '',
										),
								 array(
											'type' => 'upload',
											'name' => 'long_desc_image2',
											'label' => __( 'Image 2', SH_NAME ),
											'description' => __( 'Upload Image.', SH_NAME ),
											'default' => '',
										),
								array(
											'type' => 'textarea',
											'name' => 'long_desc',
											'label' => __('Description', SH_NAME),
											'description' => __('Enter text to be displayed as Description.', SH_NAME),
											
										),
									
								array(
									'type' => 'builder',
									'label' => __('List Items', SH_NAME ),
									'name' => 'list_items',
									'fields' => array(
										array(
											'type' => 'textbox',
											'name' => 'list_item_title',
											'label' => __('List Item Title', SH_NAME),
											'description' => __('Enter text to be displayed as List Item Title.', SH_NAME),
										),
										array(
											'type' => 'select',
											'name' => 'list_item_icon',
											'label' => __( 'List Item Icon', SH_NAME ),
											'items' => array(
														 'data' => array(
																	 array(
																		'source' => 'function',
																		'value' => 'eos_theme_icons' 
																	 ) 
																   ) 
													   ),
											'description' => __( 'Choose List Item Icon.', SH_NAME ),
										),
									   ),
									  ) 
									),							   
							), 
				array(
							'title' => __( 'Testimonials Section', SH_NAME ),
							'name' => 'testimonials_settings',
							'icon' => 'font-awesome:icon-thumbs-up-alt',
							'controls' => array(
									
								  array(
											'type' => 'upload',
											'name' => 'testimonials_section_bg',
											'label' => __( 'BG Image', SH_NAME ),
											'description' => __( 'Upload Image to use as BG image of this Section.', SH_NAME ),											'default' => '',
										),
								
								array(
									'type' => 'builder',
									'label' => __('Testimonials', SH_NAME ),
									'name' => 'testimonials',
									'fields' => array(
										array(
											'type' => 'textbox',
											'name' => 'testimonial_author',
											'label' => __('Author', SH_NAME),
											'description' => __('Enter name of Testimonial Author.', SH_NAME),
										),
										array(
											'type' => 'textbox',
											'name' => 'author_designation',
											'label' => __('Designation', SH_NAME),
											'description' => __('Enter Designation of  Author.', SH_NAME),
										),
										array(
											'type' => 'textarea',
											'name' => 'testimonial_text',
											'label' => __('Text', SH_NAME),
											'description' => __('Enter Text of Testimonial.', SH_NAME),
										),
										
										),
									  ) 
									),							   
							),							 
				array(
							'title' => __( 'Pricing Tables Settings', SH_NAME ),
							'name' => 'pricing_tables_settings',
							'icon' => 'font-awesome:icon-table',
							'controls' => array(
									
								  array(
											'type' => 'textbox',
											'name' => 'pricing_table_heading',
											'label' => __( 'Heading', SH_NAME ),
											'description' => __( 'Enter text to Display as Heading of this Section.', SH_NAME ),											'default' => '',
										),
								array(
											'type' => 'textbox',
											'name' => 'pricing_table_tagline',
											'label' => __( 'Tag Line', SH_NAME ),
											'description' => __( 'Enter text to Display as Tag Line of this Section.', SH_NAME ),											'default' => '',
										),
								array(
									'type' => 'builder',
									'label' => __('Features', SH_NAME ),
									'name' => 'pricing_table_features',
									'description' => __( 'Enter Pricing Tables Features.', SH_NAME ),	
									'fields' => array(
										array(
											'type' => 'textbox',
											'name' => 'pt_feature_title',
											'label' => __('Feature', SH_NAME),
											'description' => __('Enter Feature Text for Pricing Table.', SH_NAME),
										),
										
										),
									  ) ,
									  
								array(
									'type' => 'builder',
									'label' => __('Pricing Table', SH_NAME ),
									'name' => 'add_pricing_table',
									'description' => __( 'ADD Pricing Tables.', SH_NAME ),	
									'fields' => array(
										array(
												'type' => 'checkbox',
												'name' => 'pt_popular',
												'label' => __('Popular ?', SH_NAME),
												'description' => __('Check this if This Pricing Table is Popular.', SH_NAME),															                                                'items' => array(
															array(
																'value' => 'popular',
																'label' => __('YES', SH_NAME),
																),
															),
												),
										
										array(
											'type' => 'textbox',
											'name' => 'pt_title',
											'label' => __('Title', SH_NAME),
											'description' => __('Enter Pricing Table Title.', SH_NAME),
										),
										array(
											'type' => 'textbox',
											'name' => 'pt_tagline',
											'label' => __('Tag Line', SH_NAME),
											'description' => __('Enter Pricing Table Tag Line.', SH_NAME),
										),
										
										array(
											'type' => 'textbox',
											'name' => 'pt_price',
											'label' => __('Price', SH_NAME),
											'description' => __('Enter Pricing Table Price.', SH_NAME),
										),
										array(
											'type' => 'textbox',
											'name' => 'pt_duration',
											'label' => __('Duration', SH_NAME),
											'description' => __('Enter Pricing Table Duration.', SH_NAME),
										),
										
										 array(
												'type' => 'checkbox',
												'name' => 'pt_features_on',
												'label' => __('Feature', SH_NAME),
												'description' => __('Check Feature to include it in Pricing Table.', SH_NAME),
												'items' => array(
													'data' => array(
													array(
														'source' => 'function',
														'value' => 'za_get_features',
													),
												),
											),
										),
										
										),
									  ) 
									),							   
							),
				array(
							'title' => __( 'Screenshots Slider', SH_NAME ),
							'name' => 'screenshots_slider_settings',
							'icon' => 'font-awesome:icon-picture',
							'controls' => array(
									array(
											'type' => 'textbox',
											'name' => 'screenshots_slider_heading',
											'label' => __('Heading', SH_NAME),
											'description' => __('Enter text to be displayed as Heading of this Section.', SH_NAME),											
										),
								   array(
											'type' => 'textbox',
											'name' => 'screenshots_slider_tagline',
											'label' => __('Tag Line', SH_NAME),
											'description' => __('Enter text to be displayed as Tag line.', SH_NAME),
											
										),									
								array(
									'type' => 'builder',
									'label' => __('Screen Shots', SH_NAME ),
									'name' => 'screenshots_image',
									'fields' => array(
										  array(
											'type' => 'upload',
											'name' => 'screenshots_slider_image',
											'label' => __( 'Screen Shot', SH_NAME ),
											'description' => __( 'Upload Image.', SH_NAME ),
											'default' => '',
										),
										
										),
									  ) 
									),							   
							),
				array(
							'title' => __( 'Logo Section', SH_NAME ),
							'name' => 'logo_section_settings',
							'icon' => 'font-awesome:icon-leaf',
							'controls' => array(								
								array(
									'type' => 'builder',
									'label' => __('Logos', SH_NAME ),
									'name' => 'logo_images',
									'fields' => array(
										  array(
											'type' => 'upload',
											'name' => 'logo_image_sec',
											'label' => __( 'Logo Image', SH_NAME ),
											'description' => __( 'Upload Image.', SH_NAME ),
											'default' => '',
										),
										
										),
									  ) 
									),							   
							),
				array(
							'title' => __( 'Sign Up for Updates', SH_NAME ),
							'name' => 'signup_for_updates',
							'icon' => 'font-awesome:icon-check',
							'controls' => array(
									 array(
									 'type' => 'section',
									'title' => __( 'Signup', SH_NAME ),
									'name' => 'newsletter_signup',
									'fields' => array(
										 array(
											'type' => 'upload',
											'name' => 'newsletter_signup_section_bg',
											'label' => __( 'Section Background Image', SH_NAME ),
											'description' => __( 'Upload Image to be used as Section Background.', SH_NAME ),
											'default' => '',
										),
										
										array(
											'type' => 'textbox',
											'name' => 'newsletter_signup_heading',
											'label' => __('Heading', SH_NAME),
											'description' => __('Enter text to be displayed as App Heading.', SH_NAME),
											
										),
										array(
											'type' => 'textarea',
											'name' => 'newsletter_signup_tagline',
											'label' => __('Tag Line', SH_NAME),
											'description' => __('Enter text to be displayed as Tag Line.', SH_NAME),
										),
										
										array(
											'type' => 'radioimage',
											'name' => 'news_letter_selection',
											'label' => __( 'Newletter Type', SH_NAME ),
											'description' => __( 'Select Newsletter Type.', SH_NAME ),
											'items' => array(
														 array(
															'value' => 'feedburner',
															'label' => __('FeedBurner', SH_NAME),
															'img' => get_template_directory_uri().'/images/feedburner.jpg' ,
														),
															array(
															'value' => 'mailchimp',
															'label' => __('Mailchimp', SH_NAME),
															'img' => get_template_directory_uri().'/images/mailchimp.png' ,
														),
													), 
										),
										array(
											'type' => 'textbox',
											'name' => 'newsletter_signup_link',
											'label' => __('Sign Up Link', SH_NAME),
											'description' => __('Enter Link to for Sign Up Button.', SH_NAME),
											'dependency' => array(
																'field' => 'news_letter_selection',
																'function' => 'vp_dep_is_feedburner' 
															)
										),
										array(
											'type' => 'textbox',
											'name' => 'newsletter_signup_button_text',
											'label' => __('Button Text', SH_NAME),
											'description' => __('Enter FeedBurner Form Button Text.', SH_NAME),
											'dependency' => array(
																'field' => 'news_letter_selection',
																'function' => 'vp_dep_is_feedburner' 
															)
										),
										 array(
											'type' => 'notebox',
											'name' => 'main_description',
											'label' => __('MailChimp Instruction', SH_NAME),
											'description' => __('<br>Please copy the following code and go to "Dashboard -> MailChimp for WP -> Forms"  and paste the copied code there. and press save changes.<br><br>&lt;div class="form-group subscribe-form-input"&gt;
						
&lt;input type="email" name="EMAIL" id="mc4wp_email" class="subscribe-form-email form-control form-control-lg" placeholder="Enter your email address"  required /&gt;

&lt;button type="submit" class="subscribe-form-submit btn btn-black btn-lg" data-loading-text=""Loading..."&gt;Subscribe&lt;/button&gt;
&lt;/div>', SH_NAME),
											'status' => 'normal',
											'dependency' => array(
																'field' => 'news_letter_selection',
																'function' => 'vp_dep_is_mailchimp' 
															)
										),							
									) 
									),
								
							   
							) 
                    
                ),
               ),
				
        ),
		
		// Blog Page Settings
        array(
             'title' => __( 'Blog Settings', SH_NAME ),
            'name' => 'blog_settings',
            'icon' => 'font-awesome:icon-pinterest',
            'menus' => array(
						array(
							'title' => __('Blog Page Settings', SH_NAME),
							'name' => 'blogpage_settings',
							'icon' => 'font-awesome:icon-pinterest',
							'controls' => array(
											array(
												'type' => 'textbox',
												'name' => 'blog_header_title',
												'label' => __('Header Title', SH_NAME),
												'description' => __('Enter Title to show on Blog Page.', SH_NAME),
											),
											array(
												 'type' => 'upload',
												'name' => 'blog_bg_image',
												'label' => __( 'Blog Title Background Image', SH_NAME ),
												'default' => '' 
											),
								
										),
						),
                 
            ) 
        ),
		
		// pre-launch Page Settings
        array(
            'title' => __( 'pre-launch Page Settings', SH_NAME ),
            'name' => 'pre_launch_settings',
            'icon' => 'font-awesome:icon-sun',
            'menus' => array(
						array(
							'title' => __('pre-launch Page Settings', SH_NAME),
							'name' => 'pre-launch_setting',
							'icon' => 'font-awesome:icon-smile',
							'controls' => array(
											array(
												'type' => 'toggle',
												'name' => 'show_pre_launch_page',
												'label' => __('Show Pre-Launch', SH_NAME),
											),
											array(
												'type' => 'upload',
												'name' => 'pre_launch_logo_image',
												'label' => __( 'Pre-Launch Logo Image', SH_NAME ),
												'dependency' => array(
																	'field' => 'show_pre_launch_page',
																	'function' => 'vp_dep_boolean' 
																),
											),
											array(
												'type' => 'textbox',
												'name' => 'pre_launch_header_title',
												'label' => __('Header Title', SH_NAME),
												'description' => __('Enter Title to show on Pre-Launch Page.', SH_NAME),
												'dependency' => array(
																	'field' => 'show_pre_launch_page',
																	'function' => 'vp_dep_boolean' 
																),
											),
											array(
												'type' => 'textarea',
												'name' => 'pre_launch_text',
												'label' => __('Description', SH_NAME),
												'description' => __('Enter Description of Pre-Launch Page.', SH_NAME),
												'dependency' => array(
																	'field' => 'show_pre_launch_page',
																	'function' => 'vp_dep_boolean' 
																),
											),
											array(
												'type' => 'upload',
												'name' => 'pre_launch_back_image',
												'label' => __( 'Back Image', SH_NAME ),
												'dependency' => array(
																	'field' => 'show_pre_launch_page',
																	'function' => 'vp_dep_boolean' 
																),
											),
											array(
												'type' => 'upload',
												'name' => 'pre_launch_front_image',
												'label' => __( 'Front Image', SH_NAME ),
												'dependency' => array(
																	'field' => 'show_pre_launch_page',
																	'function' => 'vp_dep_boolean' 
																), 
											),
											array(
												'type' => 'upload',
												'name' => 'pre_launch_bg_image',
												'label' => __( 'Page Background Image', SH_NAME ),
												'dependency' => array(
																	'field' => 'show_pre_launch_page',
																	'function' => 'vp_dep_boolean' 
																), 
											),
											array(
												'type' => 'builder',
												'label' => __('Social Media', SH_NAME ),
												'name' => 'add_social_media_prelaunch',
												'fields' => array(
																array(
																	'type' => 'fontawesome',
																	'name' => 'pre_launch_font_awesome',
																	'label' => __('Font-awesome Icon', SH_NAME),
																	'description' => __('Choose Icons to Show.', SH_NAME),
																),
																array(
																	'type' => 'textbox',
																	'name' => 'pre_launch_font_awesome_link',
																	'label' => __('Icon Link', SH_NAME),
																),
															),
											),
										),
						),
                 
            ) 
        ),
       
        
    ) 
);

/**
 *EOF
 */