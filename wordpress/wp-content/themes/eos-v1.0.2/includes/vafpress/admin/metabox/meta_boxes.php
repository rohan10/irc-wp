<?php
$options = array();

/** Page Options*/
$options[] =  array(
	'id'          => ('sh_page_meta'),
	'types'       => array('page'),
	'title'       => __('Page Options', SH_NAME),
	'priority'    => 'high',
	'template'    => array(
	
		
		array(
			'type'      => 'group',
			'repeating' => false,
			'length'    => 1,
			'name'      => 'sh_page_general',
			'title'     => __('General Options', SH_NAME),
			'fields'    => array(
				
				array(
					'type' => 'checkbox',
					'name' => 'grey_bg',
					'label' => __('Grey Background ?', SH_NAME),
					'default' => '',
					'items' => array(
						array(
							'value' => 'yes',
							'label' => __('Grey BG', SH_NAME),
						),
					),
				),
				
			),
		),
	),
);

$options[] =  array(
	'id'          => ('sh_post_meta'),
	'types'       => array('post'),
	'title'       => __('Post Options', SH_NAME),
	'priority'    => 'high',
	'template'    => array(
	
		
		array(
			'type'      => 'group',
			'repeating' => false,
			'length'    => 1,
			'name'      => 'sh_post_general',
			'title'     => __('General Options', SH_NAME),
			'fields'    => array(
				array(
					'type' => 'textbox',
					'name' => 'header_text',
					'label' => __('Header Title', SH_NAME),
					'default' => '',
				),
				array(
					'type' => 'textbox',
					'name' => 'author',
					'label' => __('Author', SH_NAME),
					'default' => '',
				),
				array(
					'type' => 'upload',
					'name' => 'bg',
					'label' => __('Background', SH_NAME),
					'default' => '',
				),
				
			),
		),
	),
);

/** Team Options*/
$options[] =  array(
	'id'          => ('sh_team_info'),
	'types'       => array('sh_team'),
	'title'       => __('Team Options', SH_NAME),
	'priority'    => 'high',
	'template'    => array(
	
		
		array(
			'type'      => 'group',
			'repeating' => false,
			'length'    => 1,
			'name'      => 'sh_team_general_info',
			'title'     => __('General Information', SH_NAME),
			'fields'    => array(
				
				array(
					'type' => 'textbox',
					'name' => 'designation',
					'label' => __('Designation', SH_NAME),
					'default' => '',
				),
			),
		),
		
		array(
			'type'      => 'group',
			'repeating' => true,
			'length'    => 1,
			'name'      => 'sh_team_social',
			'title'     => __('Social Media Profile', SH_NAME),
			'fields'    => array(
				
				array(
					'type' => 'fontawesome',
					'name' => 'social_icon',
					'label' => __('Social Icon', SH_NAME),
					'default' => '',
				),
				
				array(
					'type' => 'textbox',
					'name' => 'social_link',
					'label' => __('Link', SH_NAME),
					'default' => '',
					
				),
			),
		),
	),
);

$options[] =  array(
	'id'          => _WSH()->set_meta_key('sh_services'),
	'types'       => array( 'sh_services' ),
	'title'       => __('Post Settings', SH_NAME),
	'priority'    => 'high',
	'template'    => 

			array(
				array(
					'type' => 'fontawesome',
					'name' => 'fontawesome',
					'label' => __('Service Icon', SH_NAME),
					'default' => '',
				),
	),
);

/** Portfolio Options*/
$options[] =  array(
	'id'          => ('portfolio_meta'),
	'types'       => array('sh_portfolio'),
	'title'       => __('Portfolio Options', SH_NAME),
	'priority'    => 'high',
	'template'    => array(
						array(
							'type' => 'textbox',
							'name' => 'link',
							'label' => __('View Live Link', SH_NAME),
							'default' => '',
						),
						array(
							'type' => 'textbox',
							'name' => 'likes',
							'label' => __('Number of Likes', SH_NAME),
							'default' => '',
						),
						array(
							'type' => 'textbox',
							'name' => 'likes_link',
							'label' => __('Link for Likes', SH_NAME),
							'default' => '',
						),
					),
			);

/**
 * EOF
 */
 
 
 return $options;
 
 
 
 
 
 
 
 