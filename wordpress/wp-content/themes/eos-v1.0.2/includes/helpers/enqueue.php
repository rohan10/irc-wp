<?php
class SH_Enqueue

{


	function __construct()

	{

		add_action( 'wp_enqueue_scripts', array( $this, 'sh_enqueue_scripts' ) );

		add_action( 'wp_head', array( $this, 'wp_head' ) );

		add_action( 'wp_footer', array( $this, 'wp_footer' ) );

	}


	function sh_enqueue_scripts()
	{
		global $post;
		$options = get_option(SH_NAME.'_theme_options');
		$stylesheet_name = (sh_set($options , 'theme_layouts') && sh_set($options , 'default_color_scheme')) ? sh_set($options , 'theme_layouts').'_'.sh_set($options , 'default_color_scheme').'.css' : 'style.css' ;
		$color_scheme = (sh_set($options , 'custom_color_scheme'))? sh_set($options , 'custom_color_scheme') : 'green' ; 
		$protocol = is_ssl() ? 'https' : 'http';
		$styles = array( 
						 'bootstrap' => 'css/bootstrap.min.css',
						 'fonts' => $protocol.'://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic',
						 'font-awesome.min' => 'css/font-awesome.min.css',
						 'simple-line-icons' => 'css/simple-line-icons.min.css',
						 'animate.min' => 'css/animate.min.css',
						 'owl.carousel.min' => 'css/owl.carousel.min.css',
						 'nivo-lightbox.min' => 'css/nivo-lightbox.min.css',
						 'nivo-lightbox_default' => 'css/nivo-lightbox/default.min.css',
						 'style' => 'css/'.$stylesheet_name,
						 'color_scheme' => 'css/colors/'.$color_scheme.'.css' ,
					   );						
		foreach( $styles as $name => $style )
		{

			if(strstr($style, 'http') || strstr($style, 'https') ) 
			{
				wp_enqueue_style( $name, $style);
			}

			else wp_enqueue_style( $name, SH_URL.$style);
		}

		$scripts = array( 
						  'bootstrap.min' => 'js/bootstrap.min.js',
						  'retina.min' => 'js/retina.min.js',
						  'smoothscroll.min' => 'js/smoothscroll.min.js',
						  'wow.min' => 'js/wow.min.js',
						  'jquery.nav.min' => 'js/jquery.nav.min.js',
						  'nivo-lightbox.min'=>'js/nivo-lightbox.min.js',
						  'jquery.stellar.min' => 'js/jquery.stellar.min.js',
						  'owl.carousel.min'=>'js/owl.carousel.min.js',
						  'script' => 'js/script.js',
						 );
		foreach( $scripts as $name => $js )
		{
			wp_register_script( $name, SH_URL.$js, '', '', true);
		}
		wp_enqueue_script( array(
						'jquery',
						'bootstrap.min' ,
						'retina.min',
						'smoothscroll.min',
						'wow.min',
						'jquery.nav.min',
						'nivo-lightbox.min',
						'jquery.stellar.min',
						'owl.carousel.min',
						'script',
						
						));
		
		if( is_singular() ) wp_enqueue_script('comment-reply');
		
	}
	function wp_head()
	{
		
		$options = get_option(SH_NAME.'_theme_options');
		echo $custom_css = sh_set($options , 'custom_css');
		wp_enqueue_script('jquery'); 
		
		
	}
	
	function wp_footer()

	{

		$this->sh_enqueue_scripts();

	}

}