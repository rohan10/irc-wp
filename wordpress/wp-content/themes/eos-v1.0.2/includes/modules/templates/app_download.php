<?php 
	$options = get_option(SH_NAME.'_theme_options') ;
	
	//printr($options);

?>
<section id="home" class="hero-section hero-layout-1 section section-inverse-color parallax-background"   data-stellar-background-ratio="0.4">
	<div class="black-background-overlay"></div>
    <div class="container">
    	<div class="hero-content">
<?php 
switch ($layout) :

  case "layout_1": ?>
  
            <!-- HERO TEXT -->
            <div class="hero-text wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                <!-- LOGO -->
                <img src="images/logos/hero-logo.png" alt="EOS - App Landing Page Template" class="hero-logo" />

                <!-- TAGLINE -->
                <h1 class="hero-title">Create Your App Landing Page in No Time with <strong>EOS</strong></h1>

                <!-- HERO DESCRIPTION -->
                <p class="hero-description">An easily customized app landing page template with smooth CSS3 animations and lots of pre-defined layouts.</p>

                <!-- DOWNLOAD BUTTONS -->
                <p class="download-buttons">
                    <!-- APP STORE DOWNLOAD -->
                    <a href="#please-edit-this-link" class="btn btn-app-download btn-ios">
                        <i class="fa fa-apple"></i>
                        <strong>Download App</strong> <span>from App Store</span>
                    </a>
                    <!-- PLAY STORE DOWNLOAD -->
                    <a href="#please-edit-this-link" class="btn btn-app-download btn-primary">
                        <i class="fa fa-android"></i>
                        <strong>Download App</strong> <span>from Play Store</span>
                    </a>
                </p>

            </div>

            <!-- HERO IMAGES -->
            <div class="hero-images wow layout3HeroPhone" data-wow-duration="1s" data-wow-delay="0.5s">
                <img src="images/contents/hero-phone-and-hand.png" class="hero-image parallax" alt="" data-stellar-ratio="0.7" />
            </div>
										
			
<?php
    break;
  case "layout_2": 
?>					

            <!-- HERO TEXT -->
            <div class="hero-text">
                
                <!-- LOGO -->
                <div class="hero-logo wow fadeIn" data-wow-duration="1s">
                    <img src="images/logos/hero-logo.png" alt="EOS - App Landing Page Template" />
                </div>

                <!-- TAGLINE -->
                <h1 class="hero-title wow fadeInUp" data-wow-duration="1s">A Beautiful App Landing Page Template</h1>

                <!-- DOWNLOAD BUTTONS -->
                <p class="download-buttons wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
                    <!-- APP STORE DOWNLOAD -->
                    <a href="#please-edit-this-link" class="btn btn-app-download btn-ios">
                        <i class="fa fa-apple"></i>
                        <strong>Download App</strong> <span>from App Store</span>
                    </a>
                    <!-- PLAY STORE DOWNLOAD -->
                    <a href="#please-edit-this-link" class="btn btn-app-download btn-primary">
                        <i class="fa fa-android"></i>
                        <strong>Download App</strong> <span>from Play Store</span>
                    </a>
                </p>

                <!-- WATCH THE VIDEO -->
                <a href="#video" class="hero-watch-video-link anchor-link wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s"><i class="fa fa-youtube-play"></i>Watch The Video</a>

            </div>

<?php 
    break;
  case "layout_3": ?>
  
            <!-- HERO TEXT -->
            <div class="hero-text wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                
                <!-- LOGO -->
                <img src="images/logos/hero-logo.png" alt="EOS - App Landing Page Template" class="hero-logo" />

                <!-- TAGLINE -->
                <h1 class="hero-title">Create Your App Landing Page in No Time with <strong>EOS</strong></h1>

                <!-- HERO DESCRIPTION -->
                <p class="hero-description">An easily customized app landing page template with smooth CSS3 animations and lots of pre-defined layouts.</p>

                <!-- DOWNLOAD BUTTONS -->
                <p class="download-buttons">
                    <!-- APP STORE DOWNLOAD -->
                    <a href="#please-edit-this-link" class="btn btn-app-download btn-ios">
                        <i class="fa fa-apple"></i>
                        <strong>Download App</strong> <span>from App Store</span>
                    </a>
                    <!-- PLAY STORE DOWNLOAD -->
                    <a href="#please-edit-this-link" class="btn btn-app-download btn-primary">
                        <i class="fa fa-android"></i>
                        <strong>Download App</strong> <span>on Play Store</span>
                    </a>
                </p>

            </div>

            <!-- HERO IMAGES -->
            <div class="hero-images">

                <img src="images/contents/hero-phone-back.png" class="hero-image phone-image-double phone-image-left wow fadeInRight" alt="" data-wow-duration="1s" data-wow-delay="0.5s" />
                <img src="images/contents/hero-phone-front.png" class="hero-image phone-image-double phone-image-right phone-image-front wow fadeInRight" alt="" data-wow-duration="1s" data-wow-delay="1.5s" />

            </div>

  
<?php 
    break;
  default:
    echo "Your favorite color is neither red, blue, or green!";
	
endswitch ; 
?>
		</div>
	</div>
</section>
<?php 
	 
?>
	  