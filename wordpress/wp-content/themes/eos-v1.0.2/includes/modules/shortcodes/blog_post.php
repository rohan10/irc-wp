
<?php 
$query_args = array('post_type' => 'post' , 'showposts' => $num ,'order_by' => $sort , 'order' => $order , 'category' => $cat);
$query = new WP_Query($query_args) ; $count = 1; 
ob_start(); 
?>

    <div class="heading-upper">
      <div class="container">
        <header class="heading animated out" data-delay="0" data-animation="fadeInUp" >
          <h2><?php echo esc_html($title); ?></h2>
          <p><?php echo esc_html($tagline); ?></p>
        </header>
      </div>
    </div>
    <div class="content-section no-padding-bottom">
      <div class="container">
        <div class="blog-area">
          <div class="row">
          <?php $count = 1; ?>
		    <?php if($query->have_posts()) : while($query->have_posts()) : $query->the_post();  global $post ; 
			if($count == 1) 
			$data_delay = 0 ;
			elseif($count == 2)
			$data_delay = 200 ; 
			elseif($count ==3)
			$data_delay == 400 ;
			?> 
            <div class="col-sm-4 animated out" data-delay="<?php echo esc_html($data_delay) ; ?>" data-animation="fadeInUp" >
              <div <?php post_class('blog-box'); ?>>
                <figure class="image"> <?php the_post_thumbnail('370x267'); ?> </figure>
                <div class="detail">
                  <h4><a class="blog-popup" href="<?php the_permalink(); ?>" ><?php the_title(); ?></a></h4>
                  <p class="meta"><?php _e("Posted by " , SH_NAME); ?> <?php the_author_posts_link() ; ?> - <?php echo get_the_date('Y-m-d'); ?> at <?php echo get_the_time(); ?>  </p>
                  <p><?php echo substr(strip_tags(get_the_content()) , 0 , 130); ?></p>
                  <a class="btn btn-gray btn-block blog-popup" href="<?php the_permalink(); ?>"><?php _e("READ MORE" , SH_NAME);?> <i class="icon-external-link"></i></a> </div>
              </div>
            </div>
			<?php	
			$count = ($count == 3) ? 1 : $count+1 ;
			endwhile ; endif;  wp_reset_query(); ?> 
          </div>
        </div>
      </div>
    </div>  
<?php $output = ob_get_contents(); 
  ob_end_clean(); 
  return $output ; ?>