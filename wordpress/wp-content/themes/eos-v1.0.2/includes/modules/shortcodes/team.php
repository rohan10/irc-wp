<?php 
	$images = ''; 
	ob_start() ;
?>

<div class="heading-upper">
  <div class="container">
    <header class="heading animated out" data-delay="0" data-animation="fadeInUp" >
      <h2><?php echo esc_html($title);?></h2>
      <p><?php echo esc_html($tagline);?></p>
    </header>
  </div>
</div>
<div class="content-section no-padding-bottom">
  <div class="container">
    <div class="team-area">
      <div class="row">
      
      <?php 
	  		$count = 1 ; 
            $args = array('post_type' => 'sh_team' , 'posts_per_page' => $number ) ; 
            if( $cat ) $args['tax_query'] = array(array('taxonomy' => 'team_category','field' => 'id','terms' => $cat));
            query_posts($args); 
            if(have_posts()):  while(have_posts()): the_post(); 
			if($count == 1) 
			$data_delay = 0 ;
			elseif($count == 2)
			$data_delay = 200 ; 
			elseif($count ==3)
			$data_delay == 400 ;
			elseif($count == 4)
			$data_delay = 600 ;
            $meta = get_post_meta(get_the_ID() , 'sh_team_info' ,true) ;
            $designation = sh_set(sh_set(sh_set($meta , 'sh_team_general_info') , 0), 'designation') ;
            $social_media = sh_set($meta , 'sh_team_social');
        ?>
        <div class="col-sm-3 animated out" data-delay="<?php echo esc_html($data_delay); ?>" data-animation="fadeInUp">
          <div class="team-box">
            <figure class="image"> <?php the_post_thumbnail(''); ?> </figure>
            <div class="detail">
              <h4><a href="#"><?php echo the_title(); ?></a></h4>
              <p class="designation"><?php echo esc_html($designation) ;?></p>
              <ul class="social-links">
              
              <?php foreach($social_media as $social ): ?>
                <li>
                <a class="<?php echo str_replace('sign' , 'square' , sh_set($social, 'social_icon')); ?>" href="<?php echo sh_set($social, 'social_link') ; ?>">                </a>
                </li>
              <?php endforeach; ?>
              
              </ul>
            </div>
          </div>
        </div>
        <?php 
		$count = ($count == 4) ? 1 : $count+1 ;
        endwhile ; endif ;
        wp_reset_query();
        ?>
        
      </div>
    </div>
  </div>
</div>
  
  <?php $output = ob_get_contents(); 
	  ob_end_clean(); 
	  return $output ; ?>