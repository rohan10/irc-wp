<?php ob_start(); 
$icon = str_replace('fa' , 'icon' , $icon) ;?>
<div class="col-sm-3 animated in" data-delay="0" data-animation="bounceIn" >
    <div class="stat-box text-center">
      <figure class="image"> <?php echo wp_get_attachment_image($bg , '270x182'); ?>
        <figcaption>
          <div class="iconic"><i class="<?php echo esc_html($icon); ?>"></i></div>
          <div class="h4"><a href="#"><?php echo esc_html($title); ?></a></div>
        </figcaption>
      </figure>
      <div class="detail"> <?php echo esc_html($number); ?> </div>
      <div class="stat-hover">
        <div class="iconic"><i class="<?php echo esc_html($icon); ?>"></i></div>
        <div class="h4"><a href="#"><?php echo esc_html($title); ?></a></div>
        <p><?php echo esc_html($text); ?></p>
      </div>
    </div>
</div>

<?php $output = ob_get_contents(); 
ob_end_clean();
return $output; ?>