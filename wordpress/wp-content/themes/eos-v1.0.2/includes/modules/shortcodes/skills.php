<?php ob_start(); ?>

<div class="widget">
    <h3><?php echo esc_html($title); ?></h3>
    <div id="skills" class="skills_bar">
      
      <?php echo do_shortcode($contents); ?>
      
    </div>
</div>
<?php $output = ob_get_contents(); 
	  ob_end_clean(); 
	  return $output ; ?>