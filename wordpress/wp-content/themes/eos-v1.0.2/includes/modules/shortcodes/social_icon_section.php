<?php 
ob_start();
?>
<div class="social-section">
  <div class="container">
	<ul data-delay="0" data-animation="bounceIn" class="social-links animated bounceIn in">
	  <?php echo do_shortcode($contents); ?>
	</ul>
  </div>
</div>    
<?php $output = ob_get_contents(); 
	  ob_end_clean(); 
	  return $output ; ?>