<?php 
	$images = '' ;
	$settings = get_option(SH_NAME.'_theme_options'); 
	$dark = (sh_set($settings , 'default_color_scheme') == 'dark')? 'pattern-black' : 'pattern-white'; 

ob_start() ; ?>

<article class="section" id="services">
    <div class="heading-upper">
      <div class="container">
        <header class="heading animated out" data-delay="0" data-animation="fadeInUp">
          <h2><?php echo esc_html($title); ?></h2>
          <p><?php echo esc_html($text); ?>.</p>
        </header>
      </div>
    </div>
    <div style="background-image:url(<?php echo esc_url(sh_set(wp_get_attachment_image_src($bg , 'full') , 0 ) ) ;?>);" class="content-section section-parallax <?php echo esc_html($dark); ?> ">
      <div class="container"> 
        <!-- services -->
        <div class="services-section">
          <div class="row">
			<?php 
				$count = 1; 
				$args = array('post_type' => 'sh_services' , 'posts_per_page' => $number ) ; 
				if( $cat ) $args['tax_query'] = array(array('taxonomy' => 'services_category','field' => 'id','terms' => $cat));
				query_posts($args); 
				if(have_posts()):  while(have_posts()): the_post(); 
				if($count == 1) 
				$data_delay = 0 ;
				elseif($count == 2)
				$data_delay = 200 ; 
				elseif($count == 3)
				$data_delay == 400 ;
				elseif($count == 4)
				$data_delay = 600 ;
				
		   		$meta = _WSH()->get_meta() ;
			?>
            <div class="col-sm-3 animated out" data-delay="<?php echo esc_html($data_delay); ?>" data-animation="flipInX" >
              <div class="service-box text-center">
                <div class="iconic">
					<i class="<?php echo sh_set($meta, 'fontawesome') ; ?>"></i>
				</div>
                <h4><a href="#"><?php echo get_the_title(); ?></a></h4>
                <p><?php echo substr(strip_tags(get_the_content()) , 0 , 110); ?></p>
              </div>
            </div>
			<?php 
			$count = ($count == 4) ? 1 : $count+1 ;
			endwhile ; endif ;
			wp_reset_query();
			?>
          </div>
        </div>
		<?php
		if($skills): 
			$theme_options = get_option(SH_NAME.'_theme_options');
			$skill = sh_set( sh_set( $theme_options, 'skills'), 'skills' ); 
		?>
		<div class="progress-bars">
          <div class="row">
		  <?php 
		  	foreach( (array)$skill as $s ):
			if(sh_set($s , 'tocopy')) 
				continue; 
		  ?>
            <div class="col-sm-6">
              <div class="progress">
                <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="<?php echo esc_html(sh_set($s , 'percentage')); ?>" role="progressbar" class="progress-bar" style="width:<?php echo esc_html(sh_set($s , 'percentage')); ?>;"> 
					<span><?php echo esc_html(sh_set($s , 'skill_name')); ?></span>
					<em><?php echo esc_html(sh_set($s , 'percentage')); ?></em> 
				</div>
              </div>
            </div>
		  <?php endforeach; ?>
          </div>
        </div>
		
		<?php endif; ?>
      </div>
    </div>
  </article>
  
<?php $output = ob_get_contents(); 
	  ob_end_clean(); 
	  return $output ; ?>