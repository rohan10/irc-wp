<?php ob_start(); ?>

<article style="background-image:url(<?php echo sh_set(wp_get_attachment_image_src($bg , 'full') , 0 ) ?>);" class="section section-parallax bg-primary pattern-black" id="contact">

	
	
	<?php if ($title): ?>
    
	<div class="heading-upper">
      <div class="container">
        <header data-delay="0" data-animation="fadeInUp" class="heading animated in">
          <h2><?php echo esc_html($title); ?></h2>
        </header>
      </div>
    </div>	
    <div class="content-section">
      <div class="container">
        <form class="contact-form" 
        action="<?php echo admin_url('admin-ajax.php?action=_sh_ajax_callback&amp;subaction=sh_contact_form_submit'); ?>" 
        method="post" id="ether_contact_form">
        <div class="msgs"></div>
          <ul class="row">
            <li class="col-sm-6">
              <div data-delay="0" data-animation="fadeInUp" class="user animated in">
                <input name="contact_name" type="text" placeholder="<?php _e("Name" , SH_NAME); ?>" class="form-control">
              </div>
            </li>
            <li data-delay="200" data-animation="fadeInUp" class="col-sm-6 animated in">
              <div class="envelop">
                <input name="contact_email" type="text" placeholder="<?php _e("Email Address" , SH_NAME); ?>" class="form-control">
              </div>
            </li>
            <li data-delay="0" data-animation="fadeInUp" class="col-sm-6 animated  in">
              <div class="category">
                <input name="contact_category" type="text" placeholder="<?php _e("Message Category" , SH_NAME); ?>" class="form-control">
              </div>
            </li>
            <li data-delay="200" data-animation="fadeInUp" class="col-sm-6 animated  in">
              <div class="tag">
                <input name="contact_subject" type="text" placeholder="<?php _e("Subject" , SH_NAME); ?>" class="form-control">
              </div>
            </li>
            <li data-delay="0" data-animation="fadeInUp" class="col-sm-12 animated  in">
              <textarea name="contact_message" placeholder="<?php _e("Write Your Message" , SH_NAME); ?>" class="form-control"></textarea>
            </li>
            <li data-delay="0" data-animation="fadeInUp" class="col-sm-12 animated in">
              <input type="submit" value="<?php _e("Send Message" , SH_NAME); ?>" class="btn btn-primary">
            </li>
          </ul>
        </form>
      </div>
    </div>
	
	<?php endif; ?>
	
  </article>
<script>
jQuery(document).ready(function($) {
    $('#ether_contact_form').live('submit', function(e){
	
		e.preventDefault();
		var thisform = this;
		var fields = $(this).serialize();
		var url= $(this).attr('action');
		//alert(url);
		$.ajax({
			url: url,
			type: 'POST',
			data: fields,
			success:function(res){
				//salert(res);
				$('.msgs', thisform).html(res);
			}
		});
	});
	

});
</script>
<?php 
	$output = ob_get_contents(); 
	ob_end_clean(); 
	return $output ; 
?>