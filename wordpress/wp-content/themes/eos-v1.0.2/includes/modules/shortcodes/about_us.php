<?php 
	$images = '';
	ob_start() ;
?>
<article class="section bg-gray"> 
    <div class="heading-upper">
      <div class="container">
        <header class="heading animated out" data-delay="0" data-animation="fadeInUp" >
          <h2><?php echo esc_html($title); ?></h2>
          <p><?php echo esc_html($text); ?></p>
        </header>
      </div>
    </div>
    <div class="content-section no-padding-bottom">
      <div class="container">
        <div class="about-area">
          <div class="row">
		  <?php 
		  $args = array('post_type' => 'sh_services' , 'posts_per_page' => $number ) ; 
		  if( $cat ) $args['tax_query'] = array(array('taxonomy' => 'services_category','field' => 'id','terms' => $cat));
		  query_posts($args);
		  $count = 1; 
		  if(have_posts()):  while(have_posts()): the_post(); 
		  global $post ; 
		   $meta = _WSH()->get_meta() ; 
		   $animation = ($count == 1) ? 'fadeInRight' : 'fadeInLeft' ; 
		  
		  ?>
            <div class="col-sm-6 animated out " data-delay="0" data-animation="<?php echo esc_html($animation); ?>" >
              <div class="about-box">
                <div class="iconic"> <i class="<?php echo sh_set($meta, 'fontawesome') ; ?>"></i> </div>
                <h4><?php echo get_the_title(); ?></h4>
                <p><?php echo substr(strip_tags(get_the_content()) , 0 , 100); ?></p>
              </div>
            </div>
			<?php 
			$count = ($count > 1)? 1 : $count+1; 
			endwhile ; endif ;
			wp_reset_query();
			?>
          </div>
        </div>
        <div class="image-hold text-center animated out " data-delay="0" data-animation="fadeInUpBig" > 
		<?php echo wp_get_attachment_image($bg , 'full'); ?> </div>
      </div>
    </div>
  </article>
  
<?php $output = ob_get_contents(); 
	  ob_end_clean(); 
	  return $output ; ?>