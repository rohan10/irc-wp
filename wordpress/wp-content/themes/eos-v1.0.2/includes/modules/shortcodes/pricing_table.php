<?php 
	$features = explode(',' , $fetures); 
	ob_start(); 
?>

<div class="col-sm-6 animated out" data-animation="fadeInRight" data-delay="200">
     <div class="pricing-table">
		<div class="pricing-detail">
		  <h4><?php echo esc_html($title); ?></h4>
		  <ul class="pricing-list">
		  
			<?php foreach((array)$features as $f): ?>
				<li><?php echo esc_html($f); ?></li>
			 <?php endforeach ; ?> 
			 
		  </ul>
		  <div class="price-amount"> <small><?php _e("Starts From" , SH_NAME) ; ?></small> <?php echo esc_html($price); ?> </div>
		</div>
		<div class="price-btm"> 
			<a href="<?php echo esc_url($btn_link); ?>"><?php echo esc_html($btn_text); ?></a> 
		</div>
     </div>
</div>

<?php 
	$output = ob_get_contents(); 
	ob_end_clean(); 
	return $output ;
 ?>
            
            
            
     