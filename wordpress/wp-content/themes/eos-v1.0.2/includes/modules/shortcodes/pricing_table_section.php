<?php ob_start(); ?>

<article id="pricing" class="section bg-image bg-primary" style="background-image:url(<?php echo sh_set(wp_get_attachment_image_src($bg , 'full') , 0 ); ?>);">
    <div class="heading-upper">
      <div class="container">
        <header class="heading animated out" data-animation="fadeInUp" data-delay="0">
          <h2><?php echo esc_html($title); ?></h2>
        </header>
      </div>
    </div>
    <div class="content-section">
      <div class="container">
        <div class="pricing-area">
          <div class="row">
		  	<?php echo do_shortcode($contents); ?>
		  </div>
        </div>
		<?php if($contact_strip): ?>
		<div data-delay="0" data-animation="fadeIn" class="contact-form animated fadeIn in">
          <form class="contactForm" method="post" action="<?php echo esc_url($link); ?>">
            <input type="text" placeholder="<?php echo esc_html($text); ?>" class="form-control">
            <input type="submit" value="Contact" class="btn btn-primary">
          </form>
        </div>
		<?php endif; ?>
      </div>
    </div>
</article>	
  
<?php 
	  $output = ob_get_contents(); 
	  ob_end_clean(); 
	  return $output ; 
?>
	  
	  
	  
