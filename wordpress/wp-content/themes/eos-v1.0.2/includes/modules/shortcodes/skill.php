<?php ob_start(); ?>
<small><?php echo esc_html($title); ?></small>
<div class="progress wow slideInLeft">
    <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo esc_html($percentage); ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo esc_html($percentage); ?>%;">
    <span class="skill_count"><?php echo esc_html($percentage); ?>%</span>
    </div>
</div>

<?php 
$output = ob_get_contents(); 
ob_end_clean(); 
return $output ; ?>