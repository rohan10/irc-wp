<?php $img_src = sh_set(wp_get_attachment_image_src($bg , 'full') , 0); 
ob_start();
?>

<div style="background-image:url(<?php echo sh_set(wp_get_attachment_image_src($bg , 'full') , 0 ) ?>);" class="section bg-image bg-primary">
      <div class="heading-upper">
        <div class="container">
          <header class="heading">
            <h2><?php echo esc_html($title); ?></h2>
          </header>
        </div>
      </div>
      <div class="content-section">
        <div class="container"> 
          
          <!-- Stats section -->
          <div class="stats-section">
            <div class="row">
              <?php echo do_Shortcode($contents); ?>
            </div>
          </div>
        </div>
      </div>
    </div>

<?php $output = ob_get_contents(); 
ob_end_clean(); 
return $output ; ?>




