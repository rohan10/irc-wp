<?php 
$count = 1;
$query_args = array('post_type' => 'sh_portfolio' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
if( $cat ) $query_args['tax_query'] = array(array('taxonomy' => 'portfolio_category','field' => 'id','terms' => $cat));
$query = new WP_Query($query_args) ; 
$terms = get_terms('portfolio_category');

ob_start();
?>
    <div class="heading-upper bg-image bg-primary pattern-theme" style="background-image:url(<?php echo sh_set(wp_get_attachment_image_src($bg , 'full') , 0 ) ?>);">
      <div class="container">
        <header class="heading">
          <h2><?php echo esc_html($title); ?></h2>
        </header>
		
		<?php if(!$cat) : ?>
		
        <ul class="filter-tabs">
		
          <li class="filter active" data-role="button" data-filter="all">
		  	<?php _e("All" , SH_NAME); ?>
		  </li>
		  <?php foreach($terms as $term): ?>
          <li class="filter" data-role="button" data-filter="<?php echo sh_set($term , 'slug'); ?>">
		  	<?php echo sh_set($term , 'name'); ?>
		  </li>
		  <?php endforeach; ?>
		  
        </ul>
		
		<?php endif; ?>
		
      </div>
    </div>
	
    <div class="content-section portfolio-area no-padding-bottom">
      <div class="portfolio-list">
        <ul class="filter-list">
		<?php if($query->have_posts()): while($query->have_posts()): $query->the_post();
			  global $post ; 
			  $meta = _WSH()->get_meta() ; 
			  $pro_term = wp_get_post_terms(get_the_ID() , 'portfolio_category' ); 
			  $term_slug = '' ;
			  $term_name = '' ;
			  foreach($pro_term as $t)
			  {
				  $term_slug .= ' '.sh_set($t , 'slug');
				  $term_name .= ' '.sh_set($t , 'name');
			  }
			  
			  if($count <= 4)  
			  	$class = 'grid-4' ;
			  elseif($count > 4) 
			  	$class = 'grid-3' ;
			  else 
			  	$class = '' ; 
				
			  $img_src = sh_set(wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()) , 'full') , 0);
			?>
			
          <li class="mix all <?php echo esc_html($term_slug); ?> <?php echo esc_html($class); ?>">
            <div class="portfolio-box">
              <figure class="image-hover"><?php the_post_thumbnail(); ?>
                <figcaption>
                  <div class="h4"><?php echo get_the_title(); ?></div>
                  <p><?php echo esc_html($term_name); ?></p>
                  <a href="<?php echo esc_url($img_src); ?>" class="link-pop image-popup"></a> 
				  <a href="<?php the_permalink(); ?>" class="link-zoom ajax-popup"></a> 
				</figcaption>
              </figure>
            </div>
          </li>
		  		  
		  <?php 
			$count = ($count == 7) ? 1 : $count+1 ;
			endwhile ;
			endif ;
			wp_reset_query(); 
		  ?>
		  
        </ul>
      </div>
    </div>

<?php $output = ob_get_contents(); 
	  ob_end_clean(); 
	  return $output ; ?>