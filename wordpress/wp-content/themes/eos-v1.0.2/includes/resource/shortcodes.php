<?php
$sh_sc = array();
$sh_sc['sh_about_us']	=	array(
					"name" => __("About Us", SH_NAME),
					"base" => "sh_about_us",
					"class" => "",
					"category" => __('Ether', SH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('About us Section with Services.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Title", SH_NAME),
						   "param_name" => "title",
						   "description" => __("Enter title for the Section.", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Text", SH_NAME),
						   "param_name" => "text",
						   "description" => __("Enter text for the Section.", SH_NAME)
						),
						
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "number",
						   "description" => __('Enter Number of services to show in this section.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( 
						   sh_get_categories( 
						   array( 'taxonomy' => 'services_category','hide_empty' => false) ) ),
						   
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "attach_images",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Bottom Image', SH_NAME ),
						   "param_name" => "bg",
						   "description" => __('Upload Bottom Image.', SH_NAME )
						),
						
					)
				);
$sh_sc['sh_services']	=	array(
					"name" => __("Services", SH_NAME),
					"base" => "sh_services",
					"class" => "",
					"category" => __('Ether', SH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Services Section', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Title", SH_NAME),
						   "param_name" => "title",
						   "description" => __("Enter title for the Services.", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Text", SH_NAME),
						   "param_name" => "text",
						   "description" => __("Enter text for the Services.", SH_NAME)
						),
						
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "number",
						   "description" => __('Enter Number of services to show in the Services section.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( 
						   sh_get_categories( 
						   array( 'taxonomy' => 'services_category','hide_empty' => false) ) ),
						   
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "attach_images",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Background Image', SH_NAME ),
						   "param_name" => "bg",
						   "description" => __('Upload Background Image.', SH_NAME )
						),
						array(
						   "type" => "checkbox",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Skills Bars", SH_NAME),
						   "param_name" => "skills",
						   'value' => array('Show Skill ???' => 1 ),
						   "description" => __("Choose either to show Skills Bars or not.", SH_NAME)
						),
						
					)
				);
				
$sh_sc['sh_fun_facts']	=	array(
					"name" => __("Fun Facts", SH_NAME),
					"base" => "sh_fun_facts",
					"class" => "",
					"category" => __('Ether', SH_NAME),
					"icon" => 'fa-briefcase' ,
					"as_parent" => array('only' => 'sh_fact'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
					"content_element" => true,
					"show_settings_on_create" => false,
					'description' => __('Add Fun Facts to your theme.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Title", SH_NAME),
						   "param_name" => "title",
						   "description" => __("Enter Title for Skills Section", SH_NAME)
						),
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("BG", SH_NAME),
						   "param_name" => "bg",
						   "description" => __("Upload Background Image", SH_NAME)
						),

					),
					"js_view" => 'VcColumnView'
				);
$sh_sc['sh_fact']		=	array(
					"name" => __("Fact", SH_NAME),
					"base" => "sh_fact",
					"class" => "",
					"category" => __('Ether', SH_NAME),
					"icon" => 'fa-briefcase' ,
					"as_child" => array('only' => 'sh_fun_facts'),// Use only|except attributes to limit child shortcodes (separate multiple values with comma)
					"content_element" => true,
					"show_settings_on_create" => true,
					'description' => __('Add Fact.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Title", SH_NAME),
						   "param_name" => "title",
						   "description" => __("Enter Title", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Hover Text", SH_NAME),
						   "param_name" => "text",
						   "description" => __("Enter Text for Hover", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Number", SH_NAME),
						   "param_name" => "number",
						   "description" => __("Enter Number", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Icon", SH_NAME),
						   "param_name" => "icon",
						   "value" => array_flip( (array)sh_font_awesome() ),
						   "description" => __("Choose Icon for Fact", SH_NAME)
						),
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("BG", SH_NAME),
						   "param_name" => "bg",
						   "description" => __("Upload Background Image", SH_NAME)
						),
					),
				);	
				
$sh_sc['sh_blog_post']	=	array(
					"name" => __("Blog Post", SH_NAME),
					"base" => "sh_blog_post",
					"class" => "",
					"category" => __('Ether', SH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Blog Posts Section.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title of this Section.', SH_NAME )
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Tagline', SH_NAME ),
						   "param_name" => "tagline",
						   "description" => __('Enter Tagline for this Section.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Posts to Show.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array('hide_empty' => FALSE ) ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
					)
				);		
$sh_sc['sh_team']		=	array(
					"name" => __("Team", SH_NAME),
					"base" => "sh_team",
					"class" => "",
					"category" => __('Ether', SH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Team Section.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title of this Section.', SH_NAME )
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Tagline', SH_NAME ),
						   "param_name" => "tagline",
						   "description" => __('Enter Tagline for this Section.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "number",
						   "description" => __('Enter Number of Members.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array('hide_empty' => FALSE ) ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
					)
				);
$sh_sc['sh_portfolio']	=	array(
					"name" => __("Portfolio Section", SH_NAME),
					"base" => "sh_portfolio",
					"class" => "",
					"category" => __('Ether', SH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Work Section.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title of this Section.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Posts to Show.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( 
						   sh_get_categories( 
						   array( 'taxonomy' => 'portfolio_category','hide_empty' => false) ) ),
						   
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "attach_images",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title Background Image', SH_NAME ),
						   "param_name" => "bg",
						   "description" => __('Upload Title Background Image.', SH_NAME )
						),
					)
				);
				
$sh_sc['sh_pricing_table_section']	=	array(
					"name" => __("Pricing Tables Section", SH_NAME),
					"base" => "sh_pricing_table_section",
					"class" => "",
					"category" => __('Ether', SH_NAME),
					"icon" => 'fa-briefcase' ,
					"as_parent" => array('only' => 'sh_pricing_table'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
					"content_element" => true,
					"show_settings_on_create" => false,
					'description' => __('Add Section of Pricing Tables.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Title", SH_NAME),
						   "param_name" => "title",
						   "description" => __("Enter Title for this Section", SH_NAME)
						),
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("BG", SH_NAME),
						   "param_name" => "bg",
						   "description" => __("Background Image for Pricing Table", SH_NAME)
						),
						array(
						   "type" => "checkbox",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Contact Strip", SH_NAME),
						   "param_name" => "contact_strip",
						   'value' => array('Show Contact Strip ?' => 1 ),
						   "description" => __("Choose either to show Contact Strip or not.", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Text for Contact Strip", SH_NAME),
						   "param_name" => "text",
						   "description" => __("Enter Text for Contact Strip", SH_NAME)
						),array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Link for Contact Strip", SH_NAME),
						   "param_name" => "link",
						   "description" => __("Enter Link for Contact Strip", SH_NAME)
						),
					),
					"js_view" => 'VcColumnView'
				);
$sh_sc['sh_pricing_table']			=	array(
					"name" => __("Pricing Table", SH_NAME),
					"base" => "sh_pricing_table",
					"class" => "",
					"category" => __('Ether', SH_NAME),
					"icon" => 'fa-briefcase' ,
					"as_child" => array('only' => 'sh_pricing_table_section'),// Use only|except attributes to limit child shortcodes (separate multiple values with comma)
					"content_element" => true,
					"show_settings_on_create" => false,
					'description' => __('Add Pricing Table.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Title", SH_NAME),
						   "param_name" => "title",
						   "description" => __("Enter Title of Pricing Table", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Price", SH_NAME),
						   "param_name" => "price",
						   "description" => __("Enter Price In Dollors", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Button Text", SH_NAME),
						   "param_name" => "btn_text",
						   "description" => __("Enter Text to show on Button", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Button Link", SH_NAME),
						   "param_name" => "btn_link",
						   "description" => __("Enter Link for Button", SH_NAME)
						),
						array(
						   "type" => "exploded_textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Features", SH_NAME),
						   "param_name" => "fetures",
						   "description" => __("Enter One Feature per Line", SH_NAME)
						),

					),
				);			
				
$sh_sc['sh_social_icon_section']	=	array(
					"name" => __("Social Icon Section", SH_NAME),
					"base" => "sh_social_icon_section",
					"class" => "",
					"category" => __('Ether', SH_NAME),
					"icon" => 'fa-briefcase' ,
					"as_parent" => array('only' => 'sh_social_icon'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
					"content_element" => true,
					"show_settings_on_create" => false,
					'description' => __('Add your Social Icons Section.', SH_NAME),
					"params" => array(),
					"js_view" => 'VcColumnView'
				);
$sh_sc['sh_social_icon']			=	array(
					"name" => __("Social Icon", SH_NAME),
					"base" => "sh_social_icon",
					"class" => "",
					"category" => __('Ether', SH_NAME),
					"icon" => 'fa-briefcase' ,
					"as_child" => array('only' => 'sh_social_icon_section'),// Use only|except attributes to limit child shortcodes (separate multiple values with comma)
					"content_element" => true,
					"show_settings_on_create" => false,
					'description' => __('Social Icon With Link.', SH_NAME),
					"params" => array(
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Icon", SH_NAME),
						   "param_name" => "icon",
						   "value" => array_flip( (array)sh_font_awesome() ),
						   "description" => __("Choose Social Icon", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Link", SH_NAME),
						   "param_name" => "link",
						   "description" => __("Enter Link for Socail Icon", SH_NAME)
						),

					),
				);				
					
$sh_sc['sh_contact_form']	=	array(
					"name" => __("Contact Form", SH_NAME),
					"base" => "sh_contact_form",
					"class" => "",
					"category" => __('Ether', SH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Add contact Form', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title for Contact Section.', SH_NAME )
						),
						array(
						   "type" => "attach_images",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Background Image for Contact Form', SH_NAME ),
						   "param_name" => "bg",
						   "description" => __('Upload Background Image for Contact Form.', SH_NAME )
						),
					)
				);		
				
				
				
				
				
								
$sh_sc = apply_filters( '_sh_shortcodes_array', $sh_sc );
return $sh_sc;