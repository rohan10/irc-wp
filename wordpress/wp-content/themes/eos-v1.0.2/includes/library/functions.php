<?php

function _WSH()
{
	return $GLOBALS['_sh_base'];
}

function sh_get_categories($arg = false, $by_slug = false, $show_all = true)
{
	global $wp_taxonomies;
	
	$categories = get_terms(sh_set( $arg, 'taxonomy', 'category' ), $arg);
	$cats = array();

	if( $show_all ) $cats[] = __( 'All Categories', SH_NAME );
	
	if( !is_wp_error( $categories ) ) {
	foreach($categories as $category)
	{
		if( $by_slug ) $cats[$category->slug] = $category->name;
		else $cats[$category->term_id] = $category->name;
	}
	}
	return $cats;
}

if( !function_exists( 'sh_slug' ) )
{
	function sh_slug( $string )
	{
		$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
		return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	}
}

function sh_get_sidebars($multi = false)
{
	global $wp_registered_sidebars;

	$sidebars = !($wp_registered_sidebars) ? get_option('wp_registered_sidebars') : $wp_registered_sidebars;

	if( $multi ) $data[] = array('value'=>'', 'label' => 'No Sidebar');
	else $data = array('' => __('No Sidebar', SH_NAME));
	foreach( (array)$sidebars as $sidebar)
	{
		if( $multi ) $data[] = array( 'value'=> sh_set($sidebar, 'id'), 'label' => sh_set( $sidebar, 'name') );
		else $data[sh_set($sidebar, 'id')] = sh_set($sidebar, 'name');
	}

	return $data;
}

if ( ! function_exists('character_limiter'))
{
	function character_limiter($str, $n = 500, $end_char = '&#8230;', $allowed_tags = false)
	{
		if($allowed_tags) $str = strip_tags($str, $allowed_tags);
		if (strlen($str) < $n)	return $str;
		$str = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));

		if (strlen($str) <= $n) return $str;

		$out = "";
		foreach (explode(' ', trim($str)) as $val)
		{
			$out .= $val.' ';
			
			if (strlen($out) >= $n)
			{
				$out = trim($out);
				return ( strlen($out ) == strlen($str)) ? $out : $out.$end_char;
			}		
		}
	}
}

function sh_get_posts_array( $post_type = 'post', $flip = false )
{
	global $wpdb;

	$res = $wpdb->get_results( "SELECT `ID`, `post_title` FROM `" .$wpdb->prefix. "posts` WHERE `post_type` = '$post_type' AND `post_status` = 'publish' ", ARRAY_A );
	
	$return = array();
	foreach( $res as $k => $r) {
		if( $flip ) {
			if( isset( $return[sh_set($r, 'post_title')] ) ) $return[sh_set($r, 'post_title').$k] = sh_set($r, 'ID');
			else $return[sh_set($r, 'post_title')] = sh_set( $r, 'ID' );
		}
		else $return[sh_set($r, 'ID')] = sh_set($r, 'post_title');
	}

	return $return;
}

function get_the_breadcrumb()
{
	global $wp_query;
	$queried_object = get_queried_object();
	
	$breadcrumb = '';
	$delimiter = ' / ';
	$before = '<li>';
	$after = '</li>';

	if ( ! is_home())
	{
		$breadcrumb .= '<li><a href="'.home_url().'">'.__('Home', SH_NAME).'</a></li>';
		
		/** If category or single post */
		if(is_category())
		{
			$cat_obj = $wp_query->get_queried_object();
			$this_category = get_category( $cat_obj->term_id );
	
			if ( $this_category->parent != 0 ) {
				$parent_category = get_category( $this_category->parent );
				$breadcrumb .= get_category_parents($parent_category, TRUE, $delimiter );
			}
			
			$breadcrumb .= '<li><a href="'.get_category_link(get_query_var('cat')).'">'.single_cat_title('', FALSE).'</a></li>';
		}
		elseif(is_tax())
		{
			$breadcrumb .= '<li><a href="'.get_term_link($queried_object).'">'.$queried_object->name.'</a></li>';
		}
		elseif(is_page()) /** If WP pages */
		{
			global $post;
			if($post->post_parent)
			{
                $anc = get_post_ancestors($post->ID);
                foreach($anc as $ancestor)
				{
                    $breadcrumb .= '<li><a href="'.get_permalink($ancestor).'">'.get_the_title($ancestor).'</a><span class="divider">/</span></li>';
                }
				$breadcrumb .= '<li>'.get_the_title($post->ID).'</li>';
				
            }else $breadcrumb .= '<li>'.get_the_title().'</li>';
		}
		elseif (is_singular())
		{
			if($category = wp_get_object_terms(get_the_ID(), array('category', 'portfolio_category')))
			{
				if( !is_wp_error($category) )
				{
					$breadcrumb .= '<li><a href="'.get_term_link(sh_set($category, '0')).'">'.sh_set( sh_set($category, '0'), 'name').'</a><span class="divider">/</span></li>';
					$breadcrumb .= '<li>'.get_the_title().'</li>';
				}
			}else{
				$breadcrumb .= '<li>'.get_the_title().'</li>';
			}
		}
		elseif(is_tag()) $breadcrumb .= '<li><a href="'.get_term_link($queried_object).'">'.single_tag_title('', FALSE).'</a></li>'; /**If tag template*/
		elseif(is_day()) $breadcrumb .= '<li><a href="">'.__('Archive for ', SH_NAME).get_the_time('F jS, Y').'</a></li>'; /** If daily Archives */
		elseif(is_month()) $breadcrumb .= '<li><a href="' .get_month_link(get_the_time('Y'), get_the_time('m')) .'">'.__('Archive for ', SH_NAME).get_the_time('F, Y').'</a></li>'; /** If montly Archives */
		elseif(is_year()) $breadcrumb .= '<li><a href="'.get_year_link(get_the_time('Y')).'">'.__('Archive for ', SH_NAME).get_the_time('Y').'</a></li>'; /** If year Archives */
		elseif(is_author()) $breadcrumb .= '<li><a href="'. esc_url( get_author_posts_url( get_the_author_meta( "ID" ) ) ) .'">'.__('Archive for ', SH_NAME).get_the_author().'</a></li>'; /** If author Archives */
		elseif(is_search()) $breadcrumb .= '<li>'.__('Search Results for ', SH_NAME).get_search_query().'</li>'; /** if search template */
		elseif(is_404()) $breadcrumb .= '<li>'.__('404 - Not Found', SH_NAME).'</li>'; /** if search template */
		elseif ( is_post_type_archive('product') ) 
		{
			
			$shop_page_id = woocommerce_get_page_id( 'shop' );
			if( get_option('page_on_front') !== $shop_page_id  )
			{
				$shop_page    = get_post( $shop_page_id );
				
				$_name = woocommerce_get_page_id( 'shop' ) ? get_the_title( woocommerce_get_page_id( 'shop' ) ) : '';
		
				if ( ! $_name ) {
					$product_post_type = get_post_type_object( 'product' );
					$_name = $product_post_type->labels->singular_name;
				}
		
				if ( is_search() ) {
		
					$breadcrumb .= $before . '<a href="' . get_post_type_archive_link('product') . '">' . $_name . '</a>' . $delimiter . __( 'Search results for &ldquo;', 'woocommerce' ) . get_search_query() . '&rdquo;' . $after;
		
				} elseif ( is_paged() ) {
		
					$breadcrumb .= $before . '<a href="' . get_post_type_archive_link('product') . '">' . $_name . '</a>' . $after;
		
				} else {
		
					$breadcrumb .= $before . $_name . $after;
		
				}
			}
	
		}
		else $breadcrumb .= '<li><a href="'.get_permalink().'">'.get_the_title().'</a></li>'; /** Default value */
	}

	return '<ul class="breadcrumb pull-right">'.$breadcrumb.'</ul>';
}

function sh_list_comments($comment, $args, $depth)
{
	
	
	$GLOBALS['comment'] = $comment; ?>
	 <li id="comment-<?php echo comment_ID(); ?>" class="comment-<?php echo comment_ID(); ?> comment">
     
     <div id="comment-container-<?php echo comment_ID(); ?>" class="comment-container">
                <div class="comment-avatar">
                <?php echo get_avatar('60') ; ?>
                </div>
                <div class="comment-header">
                    <span class="comment-name"><?php echo get_comment_author_link(); ?></span>
                    <span class="comment-is-author"><?php _e( '(Author)', 'willow' ); ?></span>
                     <span class="comment-reply">
			 <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
			 </span>
                    <small class="comment-date"> <?php echo get_comment_date(); ?></small>
                </div>

                <div class="comment-content">
                    <p> <?php comment_text(); /** print our comment text */ ?> </p>
                </div>
            </div>
	<li>

<?php
}

function sh_comment_form( $args = array(), $post_id = null, $review = false )
{
	if ( null === $post_id )
		$post_id = get_the_ID();
	else
		$id = $post_id;

	$commenter = wp_get_current_commenter();
	$user = wp_get_current_user();
	$user_identity = $user->exists() ? $user->display_name : '';

	$args = wp_parse_args( $args ); 
	if ( ! isset( $args['format'] ) )
		$args['format'] = current_theme_supports( 'html5', 'comment-form' ) ? 'html5' : 'xhtml';

	$req      = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true'" : '' );
	$html5    = 'html5' === $args['format'];
	$fields   =  array(
		'author' => '<div class="respond-author-field form-group">
		<input id="author respond-author" class="form-control" name="author" type="text" placeholder="'.__("Name" , SH_NAME).'" autocomplete="off" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' />
		</div>',
		
		'email'  => '<div class="respond-author-field form-group">
						<input id="email" class="form-control" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' placeholder="'.__("Email" , SH_NAME).'" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' />
					</div>',				
		'url'    => '<div class="respond-author-field last form-group">
		<input id="url" class="form-control" name="url" ' . ( $html5 ? 'type="url"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_url'] ) . '" placeholder="'.__("Website" , SH_NAME).'" size="30" />
		</div>',
	);

	$required_text = sprintf( ' ' . __('Required fields are marked %s', SH_NAME), '<span class="required">*</span>' );

	/**
	 * Filter the default comment form fields.
	 *
	 * @since 3.0.0
	 *
	 * @param array $fields The default comment fields.
	 */
	$fields = apply_filters( 'comment_form_default_fields', $fields );
	$defaults = array(
		'fields'               => $fields,
		'comment_field'        => '<div class="respond-comment form-group">
		<textarea id="comment respond-comment" placeholder="'. __( 'Enter your comment …', SH_NAME ).'" class="form-control" name="comment" cols="45" rows="8" aria-required="true" autocomplete="off"></textarea>
		</div>',
		
		'must_log_in'          => '<p>' . sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.', SH_NAME ), wp_login_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>',
		
		'logged_in_as'         => '<p>' . sprintf( __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>', SH_NAME ), get_edit_user_link(), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>',
		
		'comment_notes_before' => '<p>' . __('Your email address will not be published. Required fields are marked *.', SH_NAME ) . ( $req ? $required_text : '' ) . '</p>',
		
		'comment_notes_after'  => '<br> <p>' . sprintf( __( 'You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes: %s', SH_NAME ), ' <br> <code style="white-space:pre-line !important">' . allowed_tags() . '</code>' ) . '</p>',
		
		'id_form'              => 'respond-form',
		'id_submit'            => 'submit',
		'title_reply'          => __( 'Leave a reply', SH_NAME ),
		'title_reply_to'       => __( 'Leave a Reply to %s', SH_NAME ),
		'cancel_reply_link'    => __( 'Cancel reply', SH_NAME ),
		'label_submit'         => __( 'Post Comment', SH_NAME ),
		'format'               => 'xhtml',
	);

	$args = wp_parse_args( $args, apply_filters( 'comment_form_defaults', $defaults ) );

	?>
		<?php if ( comments_open( $post_id ) ) : ?>
			<?php
			/**
			 * Fires before the comment form.
			 *
			 * @since 3.0.0
			 */
			do_action( 'comment_form_before' );
			?>

            <div id="respond"  class="comment-respond">
              <h3 id="reply-title" class="comment-reply-title"><?php comment_form_title( $args['title_reply'], $args['title_reply_to'] ); ?> <small><?php cancel_comment_reply_link( $args['cancel_reply_link'] ); ?></small></h3>
                     
				<?php if ( get_option( 'comment_registration' ) && !is_user_logged_in() ) : ?>
					<?php echo $args['must_log_in']; ?>
					<?php
					
					do_action( 'comment_form_must_log_in_after' );
					?>
				<?php else : ?>
					<form action="<?php echo site_url( '/wp-comments-post.php' ); ?>" method="post" id="<?php echo esc_attr( $args['id_form'] ); ?>" class="comment-form" novalidate="">
						<?php
						
						do_action( 'comment_form_top' );
						?>
						<?php if ( is_user_logged_in() ) : ?>
							<?php
							
							echo apply_filters( 'comment_form_logged_in', $args['logged_in_as'], $commenter, $user_identity );
							?>
							<?php
							
							do_action( 'comment_form_logged_in_after', $commenter, $user_identity );
							?>
						<?php else : ?>
							<?php echo $args['comment_notes_before']; ?>
							<?php
							
							do_action( 'comment_form_before_fields' );
							foreach ( (array) $args['fields'] as $name => $field ) {
								
								echo apply_filters( "comment_form_field_{$name}", $field ) . "\n";
							}
							
							do_action( 'comment_form_after_fields' );
							?>
						<?php endif; ?>
						<?php
						
						echo apply_filters( 'comment_form_field_comment', $args['comment_field'] );
						?>
                       
                        
                      
						<?php echo $args['comment_notes_after']; ?>
						 <div class="col-sm-12 sendbutton">
							<button name="submit" type="submit" class="btn btn-primary" id="<?php echo esc_attr( $args['id_submit'] ); ?>" ><?php echo esc_attr( $args['label_submit'] ); ?></button>
							<?php comment_id_fields( $post_id ); ?>
						  </div>
						<?php
						/**
						 * Fires at the bottom of the comment form, inside the closing </form> tag.
						 *
						 * @since 1.5.2
						 *
						 * @param int $post_id The post ID.
						 */
						do_action( 'comment_form', $post_id );
						?>
					</form>
				<?php endif; ?>
			</div><!-- #respond -->
			<?php
			/**
			 * Fires after the comment form.
			 *
			 * @since 3.0.0
			 */
			do_action( 'comment_form_after' );
		else :
			/**
			 * Fires after the comment form if comments are closed.
			 *
			 * @since 3.0.0
			 */
			do_action( 'comment_form_comments_closed' );
		endif;
}

function sh_blog_excerpt_more( $more ) {
	return '';
}
add_filter('excerpt_more', 'sh_blog_excerpt_more');

function _the_pagination($args = array(), $echo = 1)
{
	
	global $wp_query;
	
	$default =  array('base' => str_replace( 99999, '%#%', esc_url( get_pagenum_link( 99999 ) ) ), 'format' => '?paged=%#%', 'current' => max( 1, get_query_var('paged') ),
						'total' => $wp_query->max_num_pages, 'next_text' => '&raquo;', 'prev_text' => '&laquo;', 'type'=>'list');
						
	$args = wp_parse_args($args, $default);			
	
	$pagination = paginate_links($args);
	
	if(paginate_links(array_merge(array('type'=>'array'),$args)))
	{
		if($echo) echo $pagination;
		return $pagination;
	}
}

function sh_register_dynamic_sidebar()
{
	$theme_options = get_option( SH_NAME.'_theme_options');
	$sidebars = sh_set( sh_set( $theme_options, 'dynamic_sidebar' ), 'dynamic_sidebar' );

	if( $sidebars && is_array( $sidebars ) )
	{
		foreach( $sidebars as $sidebar ){
			
			if( isset( $sidebar['tocopy'] ) ) continue;
			
			register_sidebar( array(
				'name' => $sidebar['sidebar_name'],
				'id' => sh_slug( $sidebar['sidebar_name'] ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget' => "</div>",
				'before_title' => '<h4 class="title"><span>',
				'after_title' => '</span></h4>',
			) );
		}
	}
}

function get_gravatar_url( $email, $width = 80 ) 
{
    $hash = md5( strtolower( trim ( $email ) ) );
    return 'http://gravatar.com/avatar/' . $hash.'&s='.$width;
}
function sh_get_attachment_id_by_url( $url ) {
// Split the $url into two parts with the wp-content directory as the separator
$parsed_url = explode( parse_url( WP_CONTENT_URL, PHP_URL_PATH ), $url );
 
// Get the host of the current site and the host of the $url, ignoring www
$this_host = str_ireplace( 'www.', '', parse_url( home_url(), PHP_URL_HOST ) );
$file_host = str_ireplace( 'www.', '', parse_url( $url, PHP_URL_HOST ) );
 
// Return nothing if there aren't any $url parts or if the current host and $url host do not match
if ( ! isset( $parsed_url[1] ) || empty( $parsed_url[1] ) || ( $this_host != $file_host ) ) {
return;
}
 
// Now we're going to quickly search the DB for any attachment GUID with a partial path match
// Example: /uploads/2013/05/test-image.jpg
global $wpdb;
 
$attachment = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}posts WHERE guid RLIKE %s;", $parsed_url[1] ) );
 
// Returns null if no attachment is found
return $attachment[0];
}

function tw_theme_icons( $code = false )
{
	$pattern = '/\.(icon-(?:\w+(?:-)?)+):before\s+{\s*content:\s*"(.+)"\s+}/';
	$subject = @file_get_contents(get_template_directory().'/css/simple-line-icons.min.css');

	if( !$subject ) return array();
	
	preg_match_all($pattern, $subject, $matches, PREG_SET_ORDER);
	
	$icons = array();
	foreach($matches as $match){
		$value = str_replace( 'icon-', ' ', $match[1] );
		$newcode = $match[1];
		
		//if( $code ) $icons[$match[1]] = stripslashes($match[2]);
		if( $code ) $icons[] = array( 'value'=> $newcode , 'label' => $value );
		else $icons[$newcode] = ucwords(str_replace('icon-', ' ', $newcode));//$match[2];
	}
	return $icons;
}

function eos_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'twentytwelve' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'eos_wp_title', 10, 2 );