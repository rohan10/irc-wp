<?php $options = get_option(SH_NAME.'_theme_options'); 
	  $link_images = sh_set( sh_set($options, 'link_images') , 'link_images' );
?>
			<?php if(sh_set($options , 'cs_form_include')): ?>
			<section id="contact" class="contact-section section collapse">
				<div class="container">
			
					<!-- SECTION HEADING -->
					<h2 class="section-heading text-center wow fadeIn" data-wow-duration="1s"><?php echo sh_set($options , 'cs_contant_title'); ?></h2>
					<div class="row">
						<div class="col-md-6 col-md-offset-3 text-center">
							<p class="animated wow fadeIn" data-wow-duration="1s">
								<?php echo sh_set($options , 'cs_contant_description'); ?>
							</p>
			
						</div>
					</div>
			
					<!-- CONTACT FORM -->
					<div class="row">
						<div class="col-md-6 col-md-offset-3">
							<form action="<?php echo admin_url('admin-ajax.php?action=_sh_ajax_callback&amp;subaction=sh_contact_form_submit'); ?>" method="post" id="eos_contact_form" class="contact-form wow fadeInUp" data-wow-duration="1s" role="form">
								<div class="msgs"><!-- Validation Message here --></div>
								
								<div class="form-group">
									<input type="text" class="form-control" name="contact_name" id="name" placeholder="<?php _e("Your Name" , SH_NAME); ?>" autocomplete="off">
								</div>
								
								<div class="form-group">
									<input type="email" class="form-control" name="contact_email" id="email" placeholder="<?php _e("Your Email Address" , SH_NAME); ?>" autocomplete="off">
								</div>
								
								<div class="form-group">
									<input type="text" class="form-control" name="contact_subject" id="subject" placeholder="<?php _e("Subject" , SH_NAME); ?>" autocomplete="off">
								</div>
								
								<div class="form-group">
									<textarea rows="4" class="form-control" name="contact_message" id="message" placeholder="<?php _e("Message" , SH_NAME); ?>"></textarea>
								</div>
								
								<div class="form-group">
									<button type="submit" id="contact_form_submit" name="contact_submit" class="btn btn-primary btn-block" data-loading-text="<?php _e("Loading..." , SH_NAME); ?>"><?php echo (sh_set($options , 'cs_contant_button_text')) ? sh_set($options , 'cs_contant_button_text') : __("Send Message" , SH_NAME); ?></button>
								</div>
								
							</form>
						</div>
					</div>
			
				</div>
			</section>
			<script>
			jQuery(document).ready(function($) {
				$('#eos_contact_form').live('submit', function(e){
				
					e.preventDefault();
					var thisform = this;
					var fields = $(this).serialize();
					var url= $(this).attr('action');
					//alert(url);
					$.ajax({
						url: url,
						type: 'POST',
						data: fields,
						success:function(res){
							//salert(res);
							$('.msgs', thisform).html(res);
						}
					});
				});
				
			
			});
			</script>
			<?php endif; ?>
			<footer class="footer-section section">
				<!-- CONTACT SECTION TOGGLE -->
				<?php if(sh_set($options , 'cs_form_include')): ?>
					<a href="#contact" class="contact-toggle" data-toggle="collapse"><i class="icon-envelope"></i></a>
				<?php endif; ?>
			
				<div class="container">
			
					<!-- DOWNLOAD BUTTONS -->
					<p class="download-buttons wow fadeIn" data-wow-duration="1s">
						<?php if(sh_set($options , 'cs_app_store_links')): ?>
						<!-- APP STORE DOWNLOAD -->
						<a href="<?php echo sh_set($options , 'app_store_link'); ?>" class="btn btn-app-download btn-ios">
							<i class="fa fa-apple"></i>
							<strong><?php _e("Download App" , SH_NAME) ; ?></strong> 
							<span><?php _e("from App Store" , SH_NAME) ; ?></span>
						</a>
						<?php
							endif;
							if(sh_set($options , 'cs_play_store_links')):
						?>
						<!-- PLAY STORE DOWNLOAD -->
						<a href="<?php echo sh_set($options , 'play_store_link'); ?>" class="btn btn-app-download btn-primary">
							<i class="fa fa-android"></i>
							<strong><?php _e("Download App" , SH_NAME) ; ?></strong> 
							<span><?php _e("from Play Store" , SH_NAME) ; ?></span>
						</a>
						<?php
							endif;
							if(sh_set($options , 'cs_win_store_links')):
						?>
						<!-- WINDOWS PHONE STORE DOWNLOAD -->
						<a href="<?php echo sh_set($options , 'win_store_link'); ?>" class="btn btn-app-download btn-windows-phone">
							<i class="fa fa-windows"></i>
							<strong><?php _e("Download App" , SH_NAME) ; ?></strong> 
							<span><?php _e("from Windows Store" , SH_NAME) ; ?></span>
						</a>
						<?php endif; ?>
						
						<?php
						if(sh_set($options , 'link_images_footer') > 0) :
						$count = 1 ;
						foreach($link_images as $link_image) :
						if(sh_set($link_image , 'tocopy')) break;
						if(sh_set($link_image , 'link_uploaded_image')):
						$img_id = sh_get_attachment_id_by_url(sh_set($link_image , 'link_uploaded_image'));
						$img_src = sh_set(wp_get_attachment_image_src( $img_id , '216x62') , 0);
					?>
					<a href="<?php echo sh_set($link_image , 'uploaded_image_link'); ?>">
						<img src="<?php echo $img_src ; ?>" class="link_images" alt="" class="img-responsive" />
					</a>
					<?php
						if(sh_set($options , 'link_images_footer') == $count ) break;
						$count++;
						endif;
						endforeach;
						endif;
					?>
						
					</p>
					
					<!-- SOCIAL MEDIA LINKS -->
					<?php 
						$social_icons = sh_set(sh_set($options , 'add_social_media') , 'add_social_media'); 
						if(sh_set(sh_set($social_icons , 0) , 'cs_social_media_icon')):
					?>
					<ul class="social-media-links wow fadeIn" data-wow-duration="1s">
						<?php
							foreach((array)$social_icons as $social_icon):
								if(sh_set($social_icon , 'tocopy')) break;
						?>
						<li>
							<a href="<?php echo sh_set($social_icon  , 'cs_sm_link'); ?>">
								<i class="fa <?php echo sh_set($social_icon  , 'cs_social_media_icon'); ?>"></i>
							</a>
						</li>
						<?php endforeach; ?>
					</ul>
					<?php endif; ?>
					<!-- COPYRIGHT -->
					<div class="copyright"><?php echo sh_set($options , 'copyright_text'); ?></div>
				</div>
			</footer>
		
		</div>
		
		<?php wp_footer(); ?>
		
	</body>

</html>
