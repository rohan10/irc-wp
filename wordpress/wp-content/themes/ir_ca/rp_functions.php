<?php
/**
 * Register footer widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */

function ir_ca_widget_footer_init() {
  register_sidebar( array(
    'name'          => __( 'Footer', 'ir_ca' ),
    'id'            => 'sidebar-2',
    'description'   => '',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
    ) );
}
add_action( 'widgets_init', 'ir_ca_widget_footer_init' );

/**
 * Format phone number for retailer page.
 * @return string Phone number formatted neatly
 */

function ir_format_phone_numbers( $input ) {
  $output = sprintf("%s-%s-%s",
              substr($input, 0, 3),
              substr($input, 3, 3),
              substr($input, 6));

  return $output;
}
