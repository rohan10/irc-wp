<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package inReach Canada
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php get_template_part('templates/globals/head'); ?>
</head>

<body <?php body_class(); ?>>

	<header id="masthead" class="site-header container-fluid" role="banner">
		<div class="site-branding row">
			<h1 class="site-title col-xs-12 col-md-2"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php bloginfo('template_url')?>/img/inreachlogo.svg" alt="inReach Canada"></a></h1>

      <?php wp_nav_menu( array( 
        'container' => 'false',
        'items_wrap' => '<ul class="col-md-8 site-nav cf">%3$s</ul>',
        'menu_class' => '',
        'theme_location' => 'main-primary'
        ) ); 
        ?>

        
        <div class="activate col-xs-12 col-md-2">
          <a href="#" class="btn btn-link">Business</a>
          <a href="#" class="btn btn-activate">Activate Your inReach</a>
        </div>
      </div><!-- .site-branding -->

    </header><!-- #masthead -->
