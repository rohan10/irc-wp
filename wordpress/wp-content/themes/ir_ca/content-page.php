<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package inReach Canada
 */
?>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <div class="hero container-fluid" style="background-image: url(<?php the_field('hero_image'); ?>)">
    <div class="row">
      <?php the_title( '<h1 class="col-xs-12 entry-title">', '</h1>' ); ?>
    </div>
  </div>
  <div class="entry-content">
    <?php the_content(); ?>
    <?php
    wp_link_pages( array(
      'before' => '<div class="page-links">' . __( 'Pages:', 'ir_ca' ),
      'after'  => '</div>',
      ) );
      ?>
    </div><!-- .entry-content -->
  </article><!-- #post-## -->

