  <?php
/**
 * @package inReach Canada
 * @template Hardware Content
 */


  /**
   * Storing Advanced custom field properties in variables for checks later
   */

  ?>
  <header class="entry-header">
    <div class="hero container-fluid" style="background-image: url(<?php the_field('page_hero'); ?>)">
      <div class="row">
        <h1 class="col-xs-12 entry-title">
          <?php the_title(); ?>
          <small><?php the_field('product_tagline'); ?></small>
        </h1>
      </div>
    </div>
  </header><!-- .entry-header -->

  <article id="post-<?php the_ID(); ?>" class="">
    <div class="odd">
      <div class="container">

        <div class="entry-content row">
          <div class="product--image col-xs-12 col-md-5 text-center" style="margin-top:-180px">
            <img class="img-responsive" src="<?php the_field('product_image'); ?>">
            <h5>Suggested Retail price: $<?php the_field('product_price'); ?></h5>
          </div>
          <div class="col-md-7">
            <p class="intro"><?php the_field('value_prop_intro'); ?></p>
            <?php the_field('value_proposition'); ?>
          </div>

          <footer class="entry-footer">
            <?php ir_ca_entry_footer(); ?>
          </footer><!-- .entry-footer -->

        </div>
      </div> <!-- .container -->
    </div>
    <div class="even">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-md-12">
            <?php the_field('product_features'); ?>
          </div>
          <div class="col-xs-12 col-md-12 well">
          <?php //the_field('whats_in_the_box'); ?><br>
          <small><?php the_field('technical_specs'); ?></small>
          </div>
        </div>
      </div>
    </div>
    <div class="odd">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <?php the_field('geopro_info'); ?>
          </div>
        </div>
      </div>
    </div>
    <div class="even">
      <div class="container">
        <?php the_field('pricing_table'); ?>
      </div>
    </div>
    <div class="odd">
      <div class="container">
        <h4>Extra block</h4>
      </div>
    </div>
  </article><!-- #post-## -->
