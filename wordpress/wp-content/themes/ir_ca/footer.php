<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package inReach Canada
 */
?>



<footer id="colophon" class="site-footer container-fluid" role="contentinfo">
  <div class="inner-footer row">
    <div class="col-xs-12 col-sm-3 col-sm-offset-9">
      <?php dynamic_sidebar( 'sidebar-2' ); ?>
    </div>
  </div>
  <div class="site-info row">
    <div class="inner-copyright col-xs-12">
      <?php printf( __( 'Theme: %1$s by %2$s.', 'ir_ca' ), 'inReach Canada', '<a href="http://roadpost.com" rel="designer">Roadpost Inc.</a>' ); ?>
    </div>
  </div><!-- .site-info -->
</footer><!-- #colophon -->


<?php wp_footer(); ?>

</body>
</html>
