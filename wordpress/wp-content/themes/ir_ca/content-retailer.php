  <?php
/**
 * @package inReach Canada
 */


  /**
   * Storing Advanced custom field properties in variables for checks later
   */
  
  $isEnterprise = get_field('isEnterprise'); 
  $location = get_field('google_map_location');
  $phoneNumber = get_field('phone');
  $nicePhoneNumber = ir_format_phone_numbers($phoneNumber);
?>
<article id="post-<?php the_ID(); ?>" class="col-xs-12 col-md-7">
  <header class="entry-header">
    <?php if ( $isEnterprise ) : ?>
      <span class="bg-primary">Enterprise dealer</span>
      <?php endif;
    ?>
  </header><!-- .entry-header -->

  <div class="entry-content row">
    <div class="acf-table col-xs-12">
      <address>
        <?php the_title( '<strong>', '</strong>' ); ?><br>
        <?php the_field('address'); ?><br>
        <?php the_field('city'); ?>, <?php the_field('province'); ?>, <?php the_field('postal_code'); ?><br>
        <abbr title="Phone">P:</abbr> <?php echo $nicePhoneNumber; ?><br>
        <a href="<?php the_field('website'); ?>"><?php the_field('website'); ?></a>
      </address>
    </div>

    <?php if( $location ) : ?>
      <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
      <script src="<?php bloginfo('template_url') ?>/js/retailerMap.js"></script>
      <div class="acf-map col-xs-12">
        <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
      </div>
    <?php endif; ?>

  </div>

	<footer class="entry-footer">
		<?php ir_ca_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
