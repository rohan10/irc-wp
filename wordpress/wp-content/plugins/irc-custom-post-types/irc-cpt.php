<?php
/**
 * Plugin Name: Custom Post Types for inReach Canada
 * Plugin URI: http://inreachcanada.com
 * Description: Retailer, ambassador and testimonials custom post types
 * Version: 0.0.1
 * Author: Roadpost Inc.
 * Author URI: http://www.roadpost.com
 * License: GPL2
 *
 * Copyright 2015 Roadpost Inc. (rnair@roadpost.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 2, as 
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/**
 * Add custom Hardware Post Type
 */
add_action( 'init', 'cpt_hardware' );

function cpt_hardware() {
  register_post_type( 'hardware',
    array(
      'menu_icon' => 'dashicons-smartphone',
      'labels' => array(
        'name' => __( 'Hardware' ),
        'singular_name' => __( 'Hardware' ),
        'add_new' => __( 'Add New Hardware' ),
        'add_new_item' => __( 'Add New Hardware' ),
        'edit_item' => __( 'Edit Hardware' ),
        'new_item' => __( 'New Hardware' ),
        'view_item' => __( 'View Hardware' ),
        'search_items' => __( 'Search Hardware' ),
        'not_found' => __( 'Hardware Not Found' ),
        'not_found_in_trash' => __( 'Hardware Not Found in Trash' )
      ),
      'public' => true,
      'has_archive' => true,
      // 'rewrite' => array('slug' => 'hardware'),
    )
  );
}

/**
 * Add custom Retailer Post Type, to store all Where-it-Buy links
 */

add_action( 'init', 'cpt_retail' );

function cpt_retail() {
  register_post_type( 'retailer',
    array(
      'menu_icon' => 'dashicons-location',
      'labels' => array(
        'name' => __( 'Retail Stores' ),
        'singular_name' => __( 'Retail Store' ),
        'add_new' => __( 'Add New Retail Store' ),
        'add_new_item' => __( 'Add New Retail Store' ),
        'edit_item' => __( 'Edit Retail Store' ),
        'new_item' => __( 'New Retail Store' ),
        'view_item' => __( 'View Retail Store' ),
        'search_items' => __( 'Search Retail Stores' ),
        'not_found' => __( 'Retail Store Not Found' ),
        'not_found_in_trash' => __( 'Retail Store Not Found in Trash' )
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'retailer'),
      'supports' => array( 'title', 'custom-fields' )
    )
  );
}

/**
 * Add custom Ambassador Post Type
 */
add_action( 'init', 'cpt_ambassador' );

function cpt_ambassador() {
  register_post_type( 'ambassador',
    array(
      'menu_icon' => 'dashicons-id',
      'labels' => array(
        'name' => __( 'Ambassadors' ),
        'singular_name' => __( 'Ambassador' ),
        'add_new' => __( 'Add New Ambassador' ),
        'add_new_item' => __( 'Add New Ambassador' ),
        'edit_item' => __( 'Edit Ambassador' ),
        'new_item' => __( 'New Ambassador' ),
        'view_item' => __( 'View Ambassador' ),
        'search_items' => __( 'Search Ambassadors' ),
        'not_found' => __( 'Ambassador Not Found' ),
        'not_found_in_trash' => __( 'Ambassador Not Found in Trash' )
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'ambassadors'),
    )
  );
}

/**
 * Add custom Testimonials Post Type
 */
add_action( 'init', 'cpt_testimonials' );

function cpt_testimonials() {
  register_post_type( 'testimonials',
    array(
      'menu_icon' => 'dashicons-testimonial',
      'labels' => array(
        'name' => __( 'Testimonials' ),
        'singular_name' => __( 'Testimonial' ),
        'add_new' => __( 'Add New Testimonial' ),
        'add_new_item' => __( 'Add New Testimonial' ),
        'edit_item' => __( 'Edit Testimonial' ),
        'new_item' => __( 'New Testimonial' ),
        'view_item' => __( 'View Testimonial' ),
        'search_items' => __( 'Search Testimonials' ),
        'not_found' => __( 'Testimonial Not Found' ),
        'not_found_in_trash' => __( 'Testimonial Not Found in Trash' )
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'testimonials'),
    )
  );
}

