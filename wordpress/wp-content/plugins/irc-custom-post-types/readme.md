# Custom Post Types for inReach Canada

## Introduction
inReach Canada (http://inreachcanada.com) as of January 2015 is run by Roadpost Inc. (http://roadpost.com). This plugin extends site functionality to add Custom Post Types for Retailers (where to buy locations), Ambassadors and Testimonials so that all three types of data can be stored within the WordPress database.

Since the plugin is merely using existing WordPress hooks, it must be published under the GPL2 license.

## Usage
The plugin doesn't exist outside its original instance, and meant to be bundled with the custom theme for inReachCanada.com. It initializes new Post models, but the views that go along with the templates must be created if the plugin is to be used with any other themes.

## 
